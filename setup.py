import setuptools
import versioneer
import os

setuptools.setup(
    name='lflow_extensions',
    version='1.0.0',
    author='Martin CUINET',
    author_email='martin.cuinet@gmail.com',
    description='Custom extensions for LibreFlow',
    long_description='Custom extensions for LibreFlow',
    long_description_content_type='text/markdown',
    url='https://gitlab.com/martin.cuinet/libreflow_extensions',
    license='LGPLv3+',
    classifiers=[
        'Programming Language :: Python :: 3',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)',
        'Operating System :: OS Independent',
    ],
    keywords='kabaret cgwire kitsu gazu animation pipeline libreflow extensions',
    install_requires=['libreflow>=2.2.6'],
    python_requires='>=3.8',
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},
    package_data={
        '': ['*.css', '*.png', '*.svg', '*.gif', '*.abc', '*.aep', '*.ai', '*.blend', '*.jpg', '*.kra', '*.mov', '*.psd', '*.txt', '*.usd', '*.fbx', '*.json', '*.obj'],
    },

)
