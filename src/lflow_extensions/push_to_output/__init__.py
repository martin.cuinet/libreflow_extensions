import os
import shutil
import tempfile
import textwrap
from kabaret import flow
from libreflow.baseflow.file import File, OpenWithAction, GenericRunAction



class MS_PushToOutput(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "push")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _has_output = False
    _process_push = False
    _script_path = ""
    _dest_file_path = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "MODEL_SHEET": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        # check if the current task has an output
        for oid in self._task.get_primary_files():
            f = self.root().get_object(oid)
            if f.file_type.get() == "Outputs" and f.name() == "MODEL_SHEET_OUTPUT_blend":
                self._has_output = True
                # print(f.name(), " ready to be updated")
                break

        if self._has_output and self._file.get_head_revision() and self._file.has_working_copy() == False:
            self.message.set(f'<h2>Wanna Update The Output File ?</h2>')
            self._process_push = True
            return True
        
        if self._file.has_working_copy() == True:
            self.message.set(f'<h2>You cannot push a working copy, you need to publish it first</h2>')
            self._process_push = False
            return True

        elif self._file.get_head_revision() == None:
            self.message.set(f'<h2>...There is no Revision to push...</h2>')
            self._process_push = False
            return True

        elif self._has_output == False:
            self.message.set(f'<h2>...There is no Output to Update...</h2>')
            self._process_push = False
            return True
    
    def get_buttons(self):
        if self._process_push:
            return ['Cancel', 'Ok']
        else: 
            return['Got it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]       

    def get_dest_file(self):
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            # print(file.name())
            if file.name() == ("MODEL_SHEET_OUTPUT_blend"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # print("Working copy created") 
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()
        # print(self._dest_file_path) 

        return self._dest_file_path

    def write_script(self):


        script_content = textwrap.dedent(f'''\
        import bpy

        # Set the path where you want to save the new blend file
        output_path = "\\{self._dest_file_path}"

        # Save the current scene as a new blend file
        bpy.ops.wm.save_as_mainfile(filepath=output_path)

        print(f"Scene saved as: ", output_path)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'push_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(script_content)

        # print(f"Render script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def link_ref(self):
        # Get the current ASSET oid
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(self.oid())]
        asset_oid = oids[-4]
        # print(asset_oid)

        #  Get the asset's RIG_2D_CHARS task 
        RIG_2D_CHARS_task_oid= asset_oid + "/tasks/RIG_2D_CHARS"
        R2DCH = self.root().get_object(RIG_2D_CHARS_task_oid)
        # print(R2DCH)

        # check if the R2DCH task already has the ref file
        files = R2DCH.get_files(include_links=True, file_type=None, primary_only=False)
        for file in files:
            if file.name() == "MODEL_SHEET_OUTPUT_blend":
                print("Task ", R2DCH.name(), " already has the reference file set up")
                return


        task_has_output = False
        #  Get the asset's Model Sheet's Output file
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            if file.name() == ("MODEL_SHEET_OUTPUT_blend"):
                ref_file = file
                task_has_output= True
                break
                
        if task_has_output:
            print("MS output found : ", ref_file)
            # print("Adding the ref file...")
            # print(ref_file.oid())
            R2DCH.file_refs.add_ref(ref_file.oid(), "Inputs")
            self._task.touch()
            R2DCH.touch()
            print("Supposed to be added already...")
        else:
            print("MS output not found ")

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                # print("! PUSHING !")
                # Add ref file to RIG_2D_CHARS
                self.link_ref()
                return GenericRunAction.run(self, button)
                # return super(MS_PushToOutput, self).run(button)
                
            else:
                print("Cannot find destination file to render to...")

class MS_PROPS_PushToOutput(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "push")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _has_output = False
    _process_push = False
    _script_path = ""
    _dest_file_path = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "MODEL_SHEET_PROPS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        # check if the current task has an output
        for oid in self._task.get_primary_files():
            f = self.root().get_object(oid)
            if f.file_type.get() == "Outputs" and f.name() == "MODEL_SHEET_PROPS_OUTPUT_blend":
                self._has_output = True
                # print(f.name(), " ready to be updated")
                break

        if self._has_output and self._file.get_head_revision() and self._file.has_working_copy() == False:
            self.message.set(f'<h2>Wanna Update The Output File ?</h2>')
            self._process_push = True
            return True
        
        if self._file.has_working_copy() == True:
            self.message.set(f'<h2>You cannot push a working copy, you need to publish it first</h2>')
            self._process_push = False
            return True

        elif self._file.get_head_revision() == None:
            self.message.set(f'<h2>...There is no Revision to push...</h2>')
            self._process_push = False
            return True

        elif self._has_output == False:
            self.message.set(f'<h2>...There is no Output to Update...</h2>')
            self._process_push = False
            return True
    
    def get_buttons(self):
        if self._process_push:
            return ['Cancel', 'Ok']
        else: 
            return['Got it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]       

    def get_dest_file(self):
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            # print(file.name())
            if file.name() == ("MODEL_SHEET_PROPS_OUTPUT_blend"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # print("Working copy created") 
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()
        # print(self._dest_file_path) 

        return self._dest_file_path

    def write_script(self):


        script_content = textwrap.dedent(f'''\
        import bpy

        # Set the path where you want to save the new blend file
        output_path = "\\{self._dest_file_path}"

        # Save the current scene as a new blend file
        bpy.ops.wm.save_as_mainfile(filepath=output_path)

        print(f"Scene saved as: ", output_path)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'push_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(script_content)

        # print(f"Render script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def link_ref(self):
        # Get the current ASSET oid
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(self.oid())]
        asset_oid = oids[-4]
        # print(asset_oid)

        #  Get the asset's RIG_2D_PROPS task 
        RIG_2D_PROPS_task_oid= asset_oid + "/tasks/RIG_2D_PROPS"
        GPMAT_PROPS_task_oid= asset_oid + "/tasks/GPMAT"
        PLT_PROPS_task_oid= asset_oid + "/tasks/PALETTE"
        R2DPR = self.root().get_object(RIG_2D_PROPS_task_oid)
        GPMATPR = self.root().get_object(GPMAT_PROPS_task_oid)
        PLT = self.root().get_object(PLT_PROPS_task_oid)
        # print(R2DCH)

        # check if the R2DCH and GPMATPR task already has the ref file
        R2DPR_files = R2DPR.get_files(include_links=True, file_type=None, primary_only=False)
        GPMATPR_files = GPMATPR.get_files(include_links=True, file_type=None, primary_only=False)
        PLT_files = PLT.get_files(include_links=True, file_type=None, primary_only=False)
        link_R2DPR = True
        link_GPMATPR = True
        link_PLT = True
        for file in R2DPR_files:
            if file.name() == "MODEL_SHEET_PROPS_OUTPUT_blend":
                print("Task ", R2DPR.name(), " already has the reference file set up")
                link_R2DPR = False
                break
        for file in GPMATPR_files:
            if file.name() == "MODEL_SHEET_PROPS_OUTPUT_blend":
                print("Task ", GPMATPR.name(), " already has the reference file set up")
                link_GPMATPR = False
                break
        for file in PLT_files:
            if file.name() == "MODEL_SHEET_PROPS_OUTPUT_blend":
                print("Task ", PLT.name(), " already has the reference file set up")
                link_PLT = False
                break

        task_has_output = False
        #  Get the asset's Model Sheet's Output file
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            if file.name() == ("MODEL_SHEET_PROPS_OUTPUT_blend"):
                ref_file = file
                task_has_output= True
                break
                
        if task_has_output:
            print("MS PROPS output found : ", ref_file)
            # print("Adding the ref file...")
            # print(ref_file.oid())
            if link_R2DPR:
                R2DPR.file_refs.add_ref(ref_file.oid(), "Inputs")
                R2DPR.touch()
            if link_GPMATPR:
                GPMATPR.file_refs.add_ref(ref_file.oid(), "Inputs")
                GPMATPR.touch()
            if link_PLT:
                PLT.file_refs.add_ref(ref_file.oid(), "Inputs")
                PLT.touch()

            self._task.touch()
             
            print("Supposed to be added already...")
        else:
            print("MS PROPS output not found ")

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                # print("! PUSHING !")
                # Add ref file to RIG_2D_CHARS
                self.link_ref()
                return GenericRunAction.run(self, button)
                # return super(MS_PushToOutput, self).run(button)
                
            else:
                print("Cannot find destination file to render to...")

class GPMAT_PushToOutput(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "push")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _has_output = False
    _process_push = False
    _script_path = ""
    _dest_file_path = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "GPMAT": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        # check if the current task has an output
        for oid in self._task.get_primary_files():
            f = self.root().get_object(oid)
            if f.file_type.get() == "Outputs" and f.name() == "GPMAT_OUTPUT_blend":
                self._has_output = True
                break

        if self._has_output and self._file.get_head_revision() and self._file.has_working_copy() == False:
            self.message.set(f'<h2>Wanna Update The Output File ?</h2>')
            self._process_push = True
            return True
        
        if self._file.has_working_copy() == True:
            self.message.set(f'<h2>You cannot push a working copy, you need to publish it first</h2>')
            self._process_push = False
            return True

        elif self._file.get_head_revision() == None:
            self.message.set(f'<h2>...There is no Revision to push...</h2>')
            self._process_push = False
            return True

        elif self._has_output == False:
            self.message.set(f'<h2>...There is no Output to Update...</h2>')
            self._process_push = False
            return True
    
    def get_buttons(self):
        if self._process_push:
            return ['Cancel', 'Ok']
        else: 
            return['Got it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]       

    def get_dest_file(self):
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            if file.name() == ("GPMAT_OUTPUT_blend"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()

        return self._dest_file_path

    def write_script(self):


        script_content = textwrap.dedent(f'''\
        import bpy

        # Set the path where you want to save the new blend file
        output_path = "\\{self._dest_file_path}"

        # Save the current scene as a new blend file
        bpy.ops.wm.save_as_mainfile(filepath=output_path)

        print(f"Scene saved as: ", output_path)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'push_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(script_content)

        self._script_path = render_scene_script_path
        return render_scene_script_path


    def link_ref(self):
        # Get the current ASSET oid
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(self.oid())]
        asset_oid = oids[-4]

        #  Get the asset's PALETTE task 
        PALETTE_task_oid= asset_oid + "/tasks/PALETTE"
        PLT = self.root().get_object(PALETTE_task_oid)

        # check if the PLT task already has the ref file
        files = PLT.get_files(include_links=True, file_type=None, primary_only=False)
        for file in files:
            if file.name() == "GPMAT_OUTPUT_blend":
                print("Task ", PLT.name(), " already has the reference file set up")
                return


        task_has_output = False
        #  Get the asset's GPMAT's Output file
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            if file.name() == ("GPMAT_OUTPUT_blend"):
                ref_file = file
                task_has_output= True
                break
                
        if task_has_output:
            print("GPMAT output found : ", ref_file)
            PLT.file_refs.add_ref(ref_file.oid(), "Inputs")
            self._task.touch()
            PLT.touch()
            print("Supposed to be added already...")
        else:
            print("PLT output not found ")


    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                self.link_ref()
                # return super(PLT_PushToOutput, self).run(button)
                return GenericRunAction.run(self, button)
                
            else:
                print("Cannot find destination file to render to...")

class PSD_PushToOutput(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons.flow", "photoshop")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _has_output = False
    _process_push = False
    _script_path = ""
    _dest_file_path = ""
    _json_file = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "LAYOUT_BG": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        # check if the current task has an output
        for oid in self._task.get_primary_files():
            f = self.root().get_object(oid)
            if f.file_type.get() == "Outputs" and f.name() == "LAY_BG_OUTPUT_psd":
                self._has_output = True
                break

        if self._has_output and self._file.get_head_revision() and self._file.has_working_copy() == False:
            

            # Check if the folder has a json file
            HR_path = self._file.get_head_revision().get_path().replace("\\", "/")
            parent_folder = os.path.dirname(HR_path)
            files_in_folder = os.listdir(parent_folder)
            json_files = [file for file in files_in_folder if file.endswith(".json")]
            if json_files:
                # Assuming there is at least one JSON file, get the first one
                json_file_path = os.path.join(parent_folder, json_files[0])

                print("-----------------------------")
                print("Json file : ", json_file_path)
                self._json_file = json_file_path
                self.message.set(f'<h2>Generate PSD ?</h2>')
                self._process_push = True
                return True
                
            else:

                self.message.set(f'<h2>Cannot find the json file. You need to export the layout data from the scene first</h2>')
                self._process_push = False
                return True

        if self._file.has_working_copy() == True:
            self.message.set(f'<h2>You cannot generate the PSD from a working copy, you need to publish it first</h2>')
            self._process_push = False
            return True

        elif self._file.get_head_revision() == None:
            self.message.set(f'<h2>...There is no Revision to push...</h2>')
            self._process_push = False
            return True

        elif self._has_output == False:
            self.message.set(f'<h2>...There is no Output to Update...</h2>')
            self._process_push = False
            return True
    
    def get_buttons(self):
        if self._process_push:
            return ['Cancel', 'Ok']
        else: 
            return['Got it']

    def runner_name_and_tags(self):
        return 'Photoshop', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [self._script_path]       

    def get_dest_file(self):
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            if file.name() == ("LAY_BG_OUTPUT_psd"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()

        return self._dest_file_path

    def write_script(self):

        print(self._dest_file_path)
        dest_file_path = self._dest_file_path.replace("\\", "/")

        # Modify the path so it searches for it at the blend file location
        json_path = self._json_file
        with open(json_path, 'r') as source_file:
            data_content = source_file.read()
        script_content = textwrap.dedent(f'''\
        
        function SavePSD(saveFile) {{  
            psdSaveOptions = new PhotoshopSaveOptions();   
            psdSaveOptions.embedColorProfile = true; 
            psdSaveOptions.alphaChannels = true;   
            psdSaveOptions.layers = true;
            psdSaveOptions.annotations = true;
            psdSaveOptions.spotColors = true; 
            activeDocument.saveAs(saveFile, psdSaveOptions, true, Extension.LOWERCASE);   
        }}

        function addLayerMask(){{  
            var idMk = charIDToTypeID( "Mk  " );
            var desc233 = new ActionDescriptor();
            var idNw = charIDToTypeID( "Nw  " );
            var idChnl = charIDToTypeID( "Chnl" );
            desc233.putClass( idNw, idChnl );
            var idAt = charIDToTypeID( "At  " );
            var ref5 = new ActionReference();
            var idChnl = charIDToTypeID( "Chnl" );
            var idChnl = charIDToTypeID( "Chnl" );
            var idMsk = charIDToTypeID( "Msk " );
            ref5.putEnumerated( idChnl, idChnl, idMsk );
            desc233.putReference( idAt, ref5 );
            var idUsng = charIDToTypeID( "Usng" );
            var idUsrM = charIDToTypeID( "UsrM" );
            var idRvlA = charIDToTypeID( "RvlA" );
            desc233.putEnumerated( idUsng, idUsrM, idRvlA );
            executeAction( idMk, desc233, DialogModes.NO );
        }}

        function import_PNG(PNG_path) {{  
            var sourceFile= new File(PNG_path);
            var idPlc = charIDToTypeID( "Plc " );
            var desc3 = new ActionDescriptor();
            var idnull = charIDToTypeID( "null" );
            desc3.putPath( idnull, sourceFile);
            var idFTcs = charIDToTypeID( "FTcs" );
            var idQCSt = charIDToTypeID( "QCSt" );
            var idQcsa = charIDToTypeID( "Qcsa" );
            desc3.putEnumerated( idFTcs, idQCSt, idQcsa );
            executeAction( idPlc, desc3, DialogModes.NO );
        }}

        function open_PSD(path){{  
            var sourceFile= new File(path);
            var idOpn = charIDToTypeID( "Opn " );
            var desc307 = new ActionDescriptor();
            var iddontRecord = stringIDToTypeID( "dontRecord" );
            desc307.putBoolean( iddontRecord, false );
            var idforceNotify = stringIDToTypeID( "forceNotify" );
            desc307.putBoolean( idforceNotify, true );
            var idnull = charIDToTypeID( "null" );
            desc307.putPath( idnull, sourceFile );
            var idDocI = charIDToTypeID( "DocI" );
            desc307.putInteger( idDocI, 733 );
            var idtemplate = stringIDToTypeID( "template" );
            desc307.putBoolean( idtemplate, false );
            executeAction( idOpn, desc307, DialogModes.NO );
        }}

        displayDialogs = DialogModes.NO

        {data_content}

        output_path = "{dest_file_path}"
        //alert("output_path : " + output_path)
        open_PSD(output_path);
        var template = activeDocument;
        template.resizeImage(data["resolution"][0], data["resolution"][1], 300, ResampleMethod.AUTOMATIC);
        doc = template.duplicate();
        template.close(SaveOptions.DONOTSAVECHANGES);

        for (var gp_name in data["files"]) {{
            var group = doc.layerSets.add();
            group.name = gp_name;
            addLayerMask();

            for (var i=0; i < data["files"][gp_name].length; i++) {{
                var img_path = data["image_folder"] + data["files"][gp_name][i];
                var layer = doc.artLayers.add();
                layer.move(group, ElementPlacement.INSIDE);
                import_PNG(img_path);
            }}
        }}
        var saveFile = new File(output_path);
        SavePSD(saveFile);
        doc.close(SaveOptions.DONOTSAVECHANGES);
        open_PSD(output_path);
        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'generate_psd.jsx')
        with open(render_scene_script_path, 'w') as file:
            file.write(script_content)

        self._script_path = render_scene_script_path
        return render_scene_script_path

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                print("script path : ", script_path)
                
                return GenericRunAction.run(self, button)
                # return super(PSD_PushToOutput, self).run(button)
                
            else:
                print("Cannot find destination file to render to...")

class RIG_PushToOutput(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "push")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _has_output = False
    _process_push = False
    _script_path = ""
    _dest_file_path = ""
    Log = None

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "RIG_2D_CHARS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        # check if the current task has an output
        for oid in self._task.get_primary_files():
            f = self.root().get_object(oid)
            if f.file_type.get() == "Outputs" and f.name() == "RIG_2D_CHARS_OUTPUT_blend":
                self._has_output = True
                break

        if self._has_output and self._file.get_head_revision() and self._file.has_working_copy() == False:
            self.message.set(f'<h2>Wanna Update The Output File ?</h2>')
            self._process_push = True
            return True
        
        if self._file.has_working_copy() == True:
            self.message.set(f'<h2>You cannot push a working copy, you need to publish it first</h2>')
            self._process_push = False
            return True

        elif self._file.get_head_revision() == None:
            self.message.set(f'<h2>...There is no Revision to push...</h2>')
            self._process_push = False
            return True

        elif self._has_output == False:
            self.message.set(f'<h2>...There is no Output to Update...</h2>')
            self._process_push = False
            return True
    
    def get_buttons(self):
        if self._process_push:
            return ['Cancel', 'Ok']
        else: 
            return['Got it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]       

    def get_dest_file(self):
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            if file.name() == ("RIG_2D_CHARS_OUTPUT_blend"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()

        return self._dest_file_path

    def write_script(self):


        script_content = textwrap.dedent(f'''\
        import bpy

        # Specify the collection names to delete
        collections_to_delete = ["COL_MS", "COL_REF"]

        # Create a list to store collections to be deleted
        collections_to_remove = []

        for collection in bpy.data.collections:
            for name in collections_to_delete:
                if name in collection.name:
                    
                    collections_to_remove.append(collection)

        # Remove the collected collections
        for collection in collections_to_remove:
            print("Deleting collection: ", collection.name)
            bpy.data.collections.remove(collection)

        # Set the path where you want to save the new blend file
        output_path = "\\{self._dest_file_path}"

        # Save the current scene as a new blend file
        bpy.ops.wm.save_as_mainfile(filepath=output_path)
        bpy.ops.gpco.generate_asset_gifs()
        print(f"Scene saved as: ", output_path)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'push_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(script_content)

        self._script_path = render_scene_script_path
        return render_scene_script_path

    def copy_gif_files(self,source_dir, destination_dir):
        # Scan the source directory
        for filename in os.listdir(source_dir):
            source_file_path = os.path.join(source_dir, filename)

            # Check if the file is a .gif file
            if (filename.lower().endswith('.gif') or filename.lower().endswith('.txt')) and os.path.isfile(source_file_path):
                destination_file_path = os.path.join(destination_dir, filename)

                # Copy the file to the destination directory
                shutil.copy2(source_file_path, destination_file_path)
                self.Log._log_view.append_text(f"Copied {filename} to {destination_dir}")

    def delete_gif_files(self,directory_path):
        # Scan the directory
        for filename in os.listdir(directory_path):
            file_path = os.path.join(directory_path, filename)

            # Check if the file is a .gif file
            if (filename.lower().endswith('.gif') or filename.lower().endswith('.txt')) and os.path.isfile(file_path):
                # Delete the .gif file
                os.remove(file_path)
                self.Log._log_view.append_text(f"Deleted {filename}")

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                output_dir = os.path.dirname(self._dest_file_path)
                self.Log._log_view.append_text("output_dir : " + output_dir)
                gif_source_dir = os.path.dirname(self._file.get_head_revision().get_path())
                self.Log._log_view.append_text("gif_source_dir : "+ gif_source_dir)
                self.delete_gif_files(output_dir)
                self.copy_gif_files(gif_source_dir, output_dir)
                # return super(RIG_PushToOutput, self).run(button)

                return GenericRunAction.run(self, button)
                
            else:
                self.Log._log_view.append_text("Cannot find destination file to render to...")

class RIG3D_PushToOutput(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "push")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _has_output = False
    _process_push = False
    _script_path = ""
    _dest_file_path = ""
    Log = None

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "RIG_3D_CHARS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        # check if the current task has an output
        for oid in self._task.get_primary_files():
            f = self.root().get_object(oid)
            if f.file_type.get() == "Outputs" and f.name() == "RIG_3D_CHARS_OUTPUT_blend":
                self._has_output = True
                break

        if self._has_output and self._file.get_head_revision() and self._file.has_working_copy() == False:
            self.message.set(f'<h2>Wanna Update The Output File ?</h2>')
            self._process_push = True
            return True
        
        if self._file.has_working_copy() == True:
            self.message.set(f'<h2>You cannot push a working copy, you need to publish it first</h2>')
            self._process_push = False
            return True

        elif self._file.get_head_revision() == None:
            self.message.set(f'<h2>...There is no Revision to push...</h2>')
            self._process_push = False
            return True

        elif self._has_output == False:
            self.message.set(f'<h2>...There is no Output to Update...</h2>')
            self._process_push = False
            return True
    
    def get_buttons(self):
        if self._process_push:
            return ['Cancel', 'Ok']
        else: 
            return['Got it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]       

    def get_dest_file(self):
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            if file.name() == ("RIG_3D_CHARS_OUTPUT_blend"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()

        return self._dest_file_path

    def write_script(self):


        script_content = textwrap.dedent(f'''\
        import bpy

        # Set the path where you want to save the new blend file
        output_path = "\\{self._dest_file_path}"

        # Save the current scene as a new blend file
        bpy.ops.wm.save_as_mainfile(filepath=output_path)
        # bpy.ops.gpco.generate_asset_gifs()
        print(f"Scene saved as: ", output_path)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'push_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(script_content)

        self._script_path = render_scene_script_path
        return render_scene_script_path

    def copy_gif_files(self,source_dir, destination_dir):
        # Scan the source directory
        for filename in os.listdir(source_dir):
            source_file_path = os.path.join(source_dir, filename)

            # Check if the file is a .gif file
            if (filename.lower().endswith('.gif') or filename.lower().endswith('.txt')) and os.path.isfile(source_file_path):
                destination_file_path = os.path.join(destination_dir, filename)

                # Copy the file to the destination directory
                shutil.copy2(source_file_path, destination_file_path)
                self.Log._log_view.append_text(f"Copied {filename} to {destination_dir}")

    def delete_gif_files(self,directory_path):
        # Scan the directory
        for filename in os.listdir(directory_path):
            file_path = os.path.join(directory_path, filename)

            # Check if the file is a .gif file
            if (filename.lower().endswith('.gif') or filename.lower().endswith('.txt')) and os.path.isfile(file_path):
                # Delete the .gif file
                os.remove(file_path)
                self.Log._log_view.append_text(f"Deleted {filename}")

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                output_dir = os.path.dirname(self._dest_file_path)
                self.Log._log_view.append_text("output_dir : " + output_dir)
                gif_source_dir = os.path.dirname(self._file.get_head_revision().get_path())
                self.Log._log_view.append_text("gif_source_dir : "+ gif_source_dir)
                self.delete_gif_files(output_dir)
                self.copy_gif_files(gif_source_dir, output_dir)
                # return super(RIG_PushToOutput, self).run(button)

                return GenericRunAction.run(self, button)
                
            else:
                self.Log._log_view.append_text("Cannot find destination file to render to...")

class LAYBG_PushToOutput(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "push")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _has_output = False
    _process_push = False
    _script_path = ""
    _dest_file_path = ""
    Log = None

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "LAYOUT_BG": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        # check if the current task has an output
        for oid in self._task.get_primary_files():
            f = self.root().get_object(oid)
            if f.file_type.get() == "Outputs" and f.name() == "LAY_BG_OUTPUT_blend":
                self._has_output = True
                break

        if self._has_output and self._file.get_head_revision() and self._file.has_working_copy() == False:
            self.message.set(f'<h2>Wanna Update The Output File ?</h2>')
            self._process_push = True
            return True
        
        if self._file.has_working_copy() == True:
            self.message.set(f'<h2>You cannot push a working copy, you need to publish it first</h2>')
            self._process_push = False
            return True

        elif self._file.get_head_revision() == None:
            self.message.set(f'<h2>...There is no Revision to push...</h2>')
            self._process_push = False
            return True

        elif self._has_output == False:
            self.message.set(f'<h2>...There is no Output to Update...</h2>')
            self._process_push = False
            return True
    
    def get_buttons(self):
        if self._process_push:
            return ['Cancel', 'Ok']
        else: 
            return['Got it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]       

    def get_dest_file(self):
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            if file.name() == ("LAY_BG_OUTPUT_blend"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()

        return self._dest_file_path

    def write_script(self):


        script_content = textwrap.dedent(f'''\
        import bpy

        # Set the path where you want to save the new blend file
        output_path = "\\{self._dest_file_path}"

        # Save the current scene as a new blend file
        bpy.ops.wm.save_as_mainfile(filepath=output_path)

        print(f"Scene saved as: ", output_path)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'push_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(script_content)

        self._script_path = render_scene_script_path
        return render_scene_script_path

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                # return super(RIG_PushToOutput, self).run(button)

                return GenericRunAction.run(self, button)
                
            else:
                self.Log._log_view.append_text("Cannot find destination file to render to...")




def bg_push_to_output(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(LAYBG_PushToOutput) # Get the relation
        relation.name = 'Push_To_Output_BG' # Explicitely definining its name
        return relation # Return relation
    
def rig_push_to_output(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(RIG_PushToOutput) # Get the relation
        relation.name = 'Push_To_Output_Rig' # Explicitely definining its name
        return relation # Return relation
    
def rig3d_push_to_output(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(RIG3D_PushToOutput) # Get the relation
        relation.name = 'Push_To_Output_Rig_3D' # Explicitely definining its name
        return relation # Return relation

def psd_generate_output(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(PSD_PushToOutput) # Get the relation
        relation.name = 'Generate_PSD' # Explicitely definining its name
        return relation # Return relation

def ms_push_to_output(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(MS_PushToOutput) # Get the relation
        relation.name = 'Push_To_Output_MS' # Explicitely definining its name
        return relation # Return relation
    
def ms_props_push_to_output(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(MS_PROPS_PushToOutput) # Get the relation
        relation.name = 'Push_To_Output_MS_Props' # Explicitely definining its name
        return relation # Return relation
    
def gpmat_push_to_output(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(GPMAT_PushToOutput) # Get the relation
        relation.name = 'Push_To_Output_GPMAT' # Explicitely definining its name
        return relation # Return relation

def install_extensions(session):
    return {
        'push_to_output_action': [
            ms_push_to_output,
            gpmat_push_to_output,
            psd_generate_output,
            rig_push_to_output,
            bg_push_to_output,
            ms_props_push_to_output,
            rig3d_push_to_output
        ]
    }
