import os
import tempfile
import textwrap
from xmlrpc.client import Boolean
import gazu
import re
import sys
import tkinter as tk
from kabaret import flow
from libreflow.baseflow.file import File, OpenWithAction, GenericRunAction
from libreflow.baseflow.kitsu import KitsuAPIWrapper as kitsu

        

class MS_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "MODEL_SHEET": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasWorkingCopy and hasRevision != None:

            self.message.set(f'<h2>Building this file ?</h2>') # Build the scene only if it has a working copy
            self._process_build = True
            return True

        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def write_script(self):


        # Need to get the folder according to the task, asset, etc...
        base_folder_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/"
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset_lib = lib_oid.rsplit('/', 1)[-1]
        asset_type = type_oid.rsplit('/', 1)[-1]
        asset = asset_oid.rsplit('/', 1)[-1]
        
        lineup_folder_path = (base_folder_path + asset_lib + "/" + asset_type + "/" + "LINEUP/04_REF_DESIGN/TO_PIPE")

        base_folder_path += (asset_lib + "/" + asset_type + "/" + asset + "/") 

        ref_design_folder_path = base_folder_path + "04_REF_DESIGN/TO_PIPE" 
        color_base_folder_path = base_folder_path + "05_COLOR/TO_PIPE" 
        model_sheet_base_folder_path = base_folder_path + "07_MODELSHEET/TO_PIPE"
        
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        
        bpy.context.view_layer.objects.active = bpy.context.scene.objects[0]
        bpy.ops.object.mode_set(mode='OBJECT')

        lineUp_folder_path = "{lineup_folder_path}"
        ref_design_folder_path = "{ref_design_folder_path}"
        color_base_folder_path = "{color_base_folder_path}"
        model_sheet_base_folder_path = "{model_sheet_base_folder_path}"

        # List of folder paths
        folder_paths = [lineUp_folder_path,ref_design_folder_path, color_base_folder_path, model_sheet_base_folder_path]

        # Iterate through each folder and import images as reference images (Image as Planes)
        for folder_path in folder_paths:
            if not os.path.exists(folder_path):
                print(folder_path, " doesn't exist, skipping")
                continue
            else:

                # List all files in the current folder
                image_files = [f for f in os.listdir(folder_path) if f.lower().endswith(('.png', '.jpg'))]

                if folder_path == model_sheet_base_folder_path:
                    if "COL_MS_FROM-TVPAINT" not in bpy.data.collections:
                        MS_collection = bpy.data.collections.new(name="COL_MS_FROM-TVPAINT")
                        MS_collection.color_tag = "COLOR_08"
                        bpy.data.collections["COL_MS_REFS"].children.link(MS_collection)
                    for image_file in image_files:       
                        # Set the image path for the reference image
                        image_path = os.path.join(folder_path, image_file)

                        # Load image as a reference image (Image as Plane)
                        bpy.ops.object.load_reference_image(filepath=image_path)

                        # Get the reference image object
                        reference_image = bpy.context.object
                        reference_image.name = "TURN"

                        bpy.data.images[image_files[0]].filepath = image_path
                        bpy.data.images[image_files[0]].source = "SEQUENCE"
                        reference_image.image_user.frame_duration = len(image_files)

                        # Rotate the reference image by 90 degrees on the X-axis
                        reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

                        reference_image.empty_display_size = 30

                        # unlink the object collection before moving it to the right one.
                        bpy.context.collection.objects.unlink(reference_image)  
                        MS_collection = bpy.data.collections["COL_MS_FROM-TVPAINT"]      
                        MS_collection.objects.link(reference_image)

                        bpy.context.scene.frame_set(2)
                        bpy.context.scene.frame_set(1)
                        print("Loaded image as reference image: ", image_path)
                        break

                else:
                    # Add images from the folder as reference images (Image as Planes)
                    for image_file in image_files:
                        # Set the image path for the reference image
                        image_path = os.path.join(folder_path, image_file)

                        # Load image as a reference image (Image as Plane)
                        bpy.ops.object.load_reference_image(filepath=image_path)

                        # Get the reference image object
                        reference_image = bpy.context.object
                        reference_image.name = image_file

                        # scale the empty
                        reference_image.empty_display_size = 30

                        # Rotate the reference image by 90 degrees on the X-axis
                        reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

                        # unlink the object collection before moving it to the right one.
                        bpy.context.collection.objects.unlink(reference_image)        


                        Ref_collection = bpy.data.collections["COL_MS_REFS"]
                        Ref_collection.objects.link(reference_image)

                        print("Loaded image as reference image: ", image_path)   

        # Rename MS collection with asset Name
        MS_collection = bpy.data.collections["COL_MS_CHARNAME"] 
        MS_Gp = bpy.data.objects["GP_MS_CHARNAME"] 
        col_new_name = "COL_MS_{asset}"
        gp_new_name = "GP_MS_{asset}"
        MS_collection.name = col_new_name
        MS_Gp.name = gp_new_name

        bpy.ops.wm.save_mainfile()                                         

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path


    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()

            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(MS_BuildScene, self).run(button)

class MS_PROPS_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "MODEL_SHEET_PROPS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasWorkingCopy and hasRevision != None:

            self.message.set(f'<h2>Building this file ?</h2>') # Build the scene only if it has a working copy
            self._process_build = True
            return True

        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def write_script(self):


        # Need to get the folder according to the task, asset, etc...
        base_folder_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/"
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset_lib = lib_oid.rsplit('/', 1)[-1]
        asset_type = type_oid.rsplit('/', 1)[-1]
        asset = asset_oid.rsplit('/', 1)[-1]
        
        lineup_folder_path = (base_folder_path + asset_lib + "/" + asset_type + "/" + "LINEUP/04_REF_DESIGN/TO_PIPE")

        base_folder_path += (asset_lib + "/" + asset_type + "/" + asset + "/") 

        ref_design_folder_path = base_folder_path + "04_REF_DESIGN/TO_PIPE" 
        color_base_folder_path = base_folder_path + "05_COLOR/TO_PIPE" 
        atk_base_folder_path = ref_design_folder_path + "/ATK"
        
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        
        bpy.context.view_layer.objects.active = bpy.context.scene.objects[0]
        bpy.ops.object.mode_set(mode='OBJECT')

        lineUp_folder_path = "{lineup_folder_path}"
        ref_design_folder_path = "{ref_design_folder_path}"
        color_base_folder_path = "{color_base_folder_path}"
        atk_base_folder_path = "{atk_base_folder_path}"

        # List of folder paths
        folder_paths = [lineUp_folder_path,ref_design_folder_path, color_base_folder_path, atk_base_folder_path]

        # Iterate through each folder and import images as reference images (Image as Planes)
        for folder_path in folder_paths:
            if not os.path.exists(folder_path):
                print(folder_path, " doesn't exist, skipping")
                continue
            else:

                # List all files in the current folder
                image_files = [f for f in os.listdir(folder_path) if f.lower().endswith(('.png', '.jpg'))]

                if folder_path == atk_base_folder_path:
                    for image_file in image_files:       
                        # Set the image path for the reference image
                        image_path = os.path.join(folder_path, image_file)

                        # Load image as a reference image (Image as Plane)
                        bpy.ops.object.load_reference_image(filepath=image_path)

                        # Get the reference image object
                        reference_image = bpy.context.object
                        reference_image.name = "TURN"

                        bpy.data.images[image_files[0]].filepath = image_path
                        bpy.data.images[image_files[0]].source = "SEQUENCE"
                        reference_image.image_user.frame_duration = len(image_files)

                        # Rotate the reference image by 90 degrees on the X-axis
                        reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

                        reference_image.empty_display_size = 30

                        # unlink the object collection before moving it to the right one.
                        bpy.context.collection.objects.unlink(reference_image)  
                        Ref_collection = bpy.data.collections["COL_MS_REFS"]
                        Ref_collection.objects.link(reference_image)

                        bpy.context.scene.frame_set(2)
                        bpy.context.scene.frame_set(1)
                        print("Loaded image as reference image: ", image_path)
                        break

                else:
                    # Add images from the folder as reference images (Image as Planes)
                    for image_file in image_files:
                        # Set the image path for the reference image
                        image_path = os.path.join(folder_path, image_file)

                        # Load image as a reference image (Image as Plane)
                        bpy.ops.object.load_reference_image(filepath=image_path)

                        # Get the reference image object
                        reference_image = bpy.context.object
                        reference_image.name = image_file

                        # scale the empty
                        reference_image.empty_display_size = 30

                        # Rotate the reference image by 90 degrees on the X-axis
                        reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

                        # unlink the object collection before moving it to the right one.
                        bpy.context.collection.objects.unlink(reference_image)        


                        Ref_collection = bpy.data.collections["COL_MS_REFS"]
                        Ref_collection.objects.link(reference_image)

                        print("Loaded image as reference image: ", image_path)   

        # Rename MS collection with asset Name
        MS_collection = bpy.data.collections["COL_MS_PROPNAME"] 
        MS_Gp = bpy.data.objects["GP_MS_PROPNAME"] 
        col_new_name = "COL_MS_{asset}"
        gp_new_name = "GP_MS_{asset}"
        MS_collection.name = col_new_name
        MS_Gp.name = gp_new_name

        bpy.ops.wm.save_mainfile()                                         

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path


    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()

            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(MS_BuildScene, self).run(button)

class BG_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""
    _ep_name = ""
    _seq_name = ""
    _sh_name = ""
    _refs = []
    _additional_shots = []
    _nb_frame = 0
    _frame_ranges = []

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "LAYOUT_BG": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        hasWorkingCopy = self._file.has_working_copy()
        shot_info = self.get_shot_info()
        if hasWorkingCopy and shot_info == True :

            self.message.set(f'<h2>Building this file ?</h2>') # Build the scene only if it has a working copy
            self._process_build = True
            return True

        elif hasWorkingCopy and type(shot_info) != bool:    
            self.message.set(f'<h2>You cannot build this scene as it is made from {shot_info}</h2>')
            self._process_build = False
            return True
        
        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def get_shot_info(self):
        # get episode sequence and shot name 
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(self.oid())]
        episode_oid, seq_oid, shot_oid = oids[-6:-3]
        
        self._ep_name = self.root().get_object(episode_oid).name()
        self._seq_name = self.root().get_object(seq_oid).name()
        self._sh_name = self.root().get_object(shot_oid).name()

        if kitsu.current_user_logged_in(self):
            # print("User logged in...")
            project = gazu.project.get_project_by_name("EWILAN")
            episode = gazu.shot.get_episode_by_name(project, self._ep_name)
            sequence = gazu.shot.get_sequence_by_name(project,self._seq_name,episode)
            shot = gazu.shot.get_shot_by_name(sequence, self._sh_name)
            task_type = gazu.task.get_task_type_by_name(self._task.name())
            task = gazu.task.get_task_by_name(shot,task_type)
            self._nb_frame = shot["nb_frames"]
            print(shot)
            shot_data = shot["data"]
            # Check if the shot has any "use_bg_from" data
            if "use_bg_from" in shot_data:
                print("shot has this data")
                # Check if the data is not none
                data = shot_data["use_bg_from"]
                if len(data)>1:
                    use_bg = shot["data"]["use_bg_from"]
                    return use_bg
                else:
                    self.get_refs(task, project)
                    return True
            else:
                self.get_refs(task, project)
                return True
            
    def get_refs(self, task, project):

        self._refs = []
        self._additional_shots = []
        self._frame_ranges = []

        pattern = re.compile(r'^EP\d{3}_SQ\d{3}_SH\d{4}$')
        comments = gazu.task.all_comments_for_task(task)
        if comments:
            for item in comments[-1]["checklist"]:
                if pattern.match(item["text"]):
                    self._additional_shots.append(item["text"])
                else:
                    self._refs.append(item["text"])

            # Get the frame ranges of the additional shots
            for shot in self._additional_shots:
                ep, seq, sh = shot.split("_")
                episode = gazu.shot.get_episode_by_name(project, ep)
                sequence = gazu.shot.get_sequence_by_name(project,seq,episode)
                additional_shot = gazu.shot.get_shot_by_name(sequence, sh)    
                self._frame_ranges.append({"shot": shot, "nb_frame": additional_shot["nb_frames"]})
            
            self._frame_ranges.append({"shot": self._ep_name+"_"+self._seq_name+"_"+self._sh_name, "nb_frame": self._nb_frame})
            print(self._frame_ranges)
            return True
        else:
            self._frame_ranges.append({"shot": self._ep_name+"_"+self._seq_name+"_"+self._sh_name, "nb_frame": self._nb_frame})
            print(self._frame_ranges)
            print("No comment found")
            return False
            

    def write_script(self):

        print("--------------------------------------------")

        print("Write script function")
        print("REFS : ", self._refs)
        print("ADDITIONAL_SHOTS : ", self._additional_shots)

        print("--------------------------------------------")
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        # Get the active camera object
        active_camera = bpy.context.scene.camera

        # Check if there is an active camera
        if active_camera:
            # Rename the camera to a new name (change "NewCameraName" to your desired name)
            new_camera_name = "CAM_{self._seq_name}_{self._sh_name}"
            active_camera.name = new_camera_name

        # Set the frame range
        start_frame = 1
        end_frame = max(entry['nb_frame'] for entry in {self._frame_ranges})

        # Set the frame start and end for the current scene
        bpy.context.scene.frame_start = start_frame
        bpy.context.scene.frame_end = end_frame

        # Set the path to the movie clip
        movie_clip_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/{self._ep_name}/ANIMATIC/TO_PIPE/MOV/{self._seq_name}_{self._sh_name}.mov"

        # Set the camera as the active object
        bpy.context.view_layer.objects.active = bpy.context.scene.camera

        # Add a background image to the camera
        background_image = active_camera.data.background_images.new()

        background_image.clip = bpy.data.movieclips.load(movie_clip_path)

        # Set the background image options
        background_image.frame_method = 'FIT'
        background_image.source = 'MOVIE_CLIP'

        
        # Create new Camera(s) if needed
        print("Additional shots : ", {self._additional_shots})
        for shot in {self._additional_shots}:
            # Create a new camera
            sq_index = shot.find("SQ")
            if sq_index != -1:
                result = shot[sq_index:]
            
            duplicated_camera = active_camera.copy()
            duplicated_camera.data = active_camera.data.copy()
            duplicated_camera.name = "CAM_"+result

            # Put the new camera in the collection "COL-RDR_CAM_BUILD"
            Cam_collection = bpy.data.collections["COL-RDR_CAM_BUILD"]
            Cam_collection.objects.link(duplicated_camera)

            # Modifiy the background clip for the new camera
            movie_clip_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/{self._ep_name}/ANIMATIC/TO_PIPE/MOV/"+result+".mov"
            background_image = duplicated_camera.data.background_images[0]
            background_image.clip = bpy.data.movieclips.load(movie_clip_path)

                
        # Import ref image
        for ref in {self._refs}:
            ref_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/{self._ep_name}/BG/" + ref + "/04_REF_DESIGN/TO_PIPE"
            print("ref_path : ", ref_path)
            image_files = [f for f in os.listdir(ref_path) if f.lower().endswith(('.png', '.jpg'))]
            for image_file in image_files:
                # Set the image path for the reference image
                image_path = os.path.join(ref_path, image_file)

                # Load image as a reference image (Image as Plane)
                bpy.ops.object.load_reference_image(filepath=image_path)

                # Get the reference image object
                reference_image = bpy.context.object
                reference_image.name = image_file

                # scale the empty
                reference_image.empty_display_size = 20

                # Rotate the reference image by 90 degrees on the X-axis
                reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

                # unlink the object collection before moving it to the right one.
                bpy.context.collection.objects.unlink(reference_image)        

                Ref_collection = bpy.data.collections["COL_REF"]
                Ref_collection.objects.link(reference_image)
        

        # set the timeline markers 
        for entry in {self._frame_ranges}:
            shot_name = entry['shot']
            frame_number = entry['nb_frame']

            # Create a timeline marker
            bpy.context.scene.timeline_markers.new(name=shot_name, frame=frame_number)
                
        bpy.ops.wm.save_mainfile()                                   

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path


    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()
            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(BG_BuildScene, self).run(button)

class AN_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    shot_info = []
    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""
    _ep_name = ""
    _seq_name = ""
    _sh_name = ""
    _refs = []
    Log = None
    _asset_array = []
    use_bg = ""

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "LAYOUT_POSING": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def ensure_assets(self, asset): 
        check = False
        for asset_oid in self.root().project().get_entity_manager().assets.mapped_names():
            if asset_oid.rsplit('/', 1)[-1] == asset:    
                self.Log._log_view.append_text(f"Asset :  {asset_oid}  Exists !")                
                check = True
                break
        return check  

    def needs_dialog(self):
        self.Log._log_view.clear()
        nb_not_found_assets = 0
        hasWorkingCopy = self._file.has_working_copy()
        self.shot_info = self.get_shot_info()
        msg = f'<h1>-- Build Scene Layout Posing --</h1>'
        msg+=f'<h2>Characters : </h2>'
        for char in self.shot_info[0]:
            check = self.ensure_assets(char)
            if check:
                msg+= f'<h4 style="color: #35db72; font-weight: 800">{char}</h4>'
            else:
                msg+= f'<h4 style="color: #db3b35; font-weight: 800">{char}</h4>'
                nb_not_found_assets +=1
            

        msg += f'<h2>Creatures : </h2>'
        for crea in self.shot_info[1]:
            check = self.ensure_assets(crea)
            if check:
                msg+= f'<h4 style="color: #35db72; font-weight: 800">{crea}</h4>'
            else:
                msg+= f'<h4 style="color: #db3b35; font-weight: 800">{crea}</h4>'
                nb_not_found_assets +=1

        msg += f'<h2>Props : </h2>'
        for prop in self.shot_info[2]:
            check = self.ensure_assets(prop)
            if check:
                msg+= f'<h4 style="color: #35db72; font-weight: 800">{prop}</h4>'
            else:
                msg+= f'<h4 style="color: #db3b35; font-weight: 800">{prop}</h4>'
                nb_not_found_assets +=1

        if nb_not_found_assets != 0:
            msg+=f'<h1>{nb_not_found_assets} Asset(s) not Found</h1>'
            msg+=f'<h2>Process anyway ?</h2>'
        else:
            msg+=f'<h1>All Assets Found</h1>'
            msg+=f'<h2>Build Scene ? ?</h2>'

        if hasWorkingCopy:
            
            self.message.set(msg) # Build the scene only if it has a working copy
            self._process_build = True
            return True

        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            self.Log._log_view.append_text("Build requested ...")
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def get_shot_info(self):
        # get episode sequence and shot name 
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(self.oid())]
        episode_oid, seq_oid, shot_oid = oids[-6:-3]
        
        self._ep_name = self.root().get_object(episode_oid).name()
        self._seq_name = self.root().get_object(seq_oid).name()
        self._sh_name = self.root().get_object(shot_oid).name()

        if kitsu.current_user_logged_in(self):
            # print("User logged in...")
            project = gazu.project.get_project_by_name("EWILAN")
            episode = gazu.shot.get_episode_by_name(project, self._ep_name)
            sequence = gazu.shot.get_sequence_by_name(project,self._seq_name,episode)
            shot = gazu.shot.get_shot_by_name(sequence, self._sh_name)

            shot_data = shot["data"]
            # Check if the shot has any "use_bg_from" data
            if "use_bg_from" in shot_data:
                self.Log._log_view.append_text("shot has this data")
                # Check if the data is not none
                data = shot_data["use_bg_from"]
                if len(data)>1:
                    self.use_bg = shot["data"]["use_bg_from"]
                    self.Log._log_view.append_text("Using BG from " + self.use_bg)
                else:
                    self.use_bg = False
                    self.Log._log_view.append_text("Using BG from same shot")

            else:
                self.use_bg = False  

            casting = gazu.casting.get_shot_casting(shot)
            self.Log._log_view.append_text(f"CASTING : {casting}")
            # print("EP : ", self._ep_name)
            # print("SEQ : ", self._seq_name)
            # print("SH : ", self._sh_name)
            characters_list = []
            creatures_list = []
            props_list = []

            # Iterate through the casting list
            for entry in casting:
                asset_type_name = entry.get('asset_type_name', '')
                # print("Asset in Casting : ",entry)
                # Check the asset_type_name and append to the respective list
                if asset_type_name == 'CHARACTERS':
                    for x in range(entry["nb_occurences"]):
                        characters_list.append(entry["asset_name"])
                elif asset_type_name == 'PROPS':
                    for y in range(entry["nb_occurences"]):
                        props_list.append(entry["asset_name"])
                elif asset_type_name == 'CREATURES':
                    for z in range(entry["nb_occurences"]):
                        creatures_list.append(entry["asset_name"])    
            
            # print("CHARACTERS : ", characters_list)
            # print("CREATURES : ", creatures_list)
            # print("PROPS : ", props_list)
            self.Log._log_view.append_text(f"CHARACTERS : {characters_list}")
            self.Log._log_view.append_text(f"CREATURES : {creatures_list}")
            self.Log._log_view.append_text(f"PROPS : {props_list}")
            return [characters_list, creatures_list, props_list]       

    def get_asset_paths(self, shot_info):
        '''
        TODO Here we need to check in which array we are (crea, chara, props)
        so we can adapt the task oid to get the asset to import. 
        '''
        self._asset_array = []
        self.Log._log_view.append_text(f"....Trying to get the asset paths with their oid...")
        for assets_array in shot_info: # for each array in the main array
            for asset in assets_array: # for each asset in the sub array
                for asset_oid in self.root().project().get_entity_manager().assets.mapped_names():
                    if asset_oid.rsplit('/', 1)[-1] == asset:
                        self.Log._log_view.append_text(f"Asset oid : " + asset_oid)
                        outputRigOid = asset_oid + "/tasks/RIG_2D_CHARS/files/RIG_2D_CHARS_OUTPUT_blend"
                        self.Log._log_view.append_text(f"outputRigOid : " + outputRigOid)
                        outputRigfile = self.root().get_object(outputRigOid)
                        if outputRigfile:
                            outputRigfilePath = outputRigfile.get_head_revision().get_path()
                            self.Log._log_view.append_text(f"outputRigfilePath : " + outputRigfilePath)
                            self._asset_array.append([asset.replace("_", "-"), outputRigfilePath.replace("/", "\\")])

        return self._asset_array

    def get_bg_path(self, use_bg):
        self.Log._log_view.append_text("-------------------------")
        self.Log._log_view.append_text("")
        self.Log._log_view.append_text(f"use_bg : {use_bg}")
        if use_bg == False:
            self.Log._log_view.append_text(f"Will use background from same shot")
            file_oid = self._file.oid()
            self.Log._log_view.append_text(f"file_oid {file_oid}")

            desired_level = 8

            # Split the path into components
            path_components = file_oid.split("/")

            # Join the components up to the desired level
            base_oid = "/".join(path_components[:desired_level + 1])
            outputLayBgfile = base_oid + "/LAYOUT_BG/files/LAY_BG_OUTPUT_blend"
            self.Log._log_view.append_text(f"desired_path {outputLayBgfile}")
            outputLayBgfilePath = self.root().get_object(outputLayBgfile).get_head_revision().get_path()
            self.Log._log_view.append_text(f"output Lay Bg file Path {outputLayBgfilePath}")

        else:
            self.Log._log_view.append_text(f"Let's go find the layout from {use_bg}")
            ep,seq, sh = use_bg.split("_")
            self.Log._log_view.append_text(f"ep {ep}")
            self.Log._log_view.append_text(f"seq {seq}")
            self.Log._log_view.append_text(f"sh {sh}")
            outputLayBgfile = f"/EWILAN/films/{ep}/sequences/{seq}/shots/{sh}/tasks/LAYOUT_BG/files/LAY_BG_OUTPUT_blend"
            self.Log._log_view.append_text(f"Shot Lay Bg File oid {outputLayBgfile}")
            outputLayBgfilePath = self.root().get_object(outputLayBgfile).get_head_revision().get_path()
            self.Log._log_view.append_text(f"output Lay Bg file Path {outputLayBgfilePath}")


        self.Log._log_view.append_text("")
        self.Log._log_view.append_text("-------------------------")
        return outputLayBgfilePath
        
        # Here we need to get the layout bg file path from use_bg
        # Whether we use the same shot layout bg file
        # Or we use the referenced Layout BG file from Kitsu bg-from
        

    def write_script(self, shot_info):
        
        asset_paths = self.get_asset_paths(shot_info)
        bg_path = self.get_bg_path(self.use_bg).replace("\\", "\\\\")
        build_scene_script_content = textwrap.dedent(f'''\
            
            import bpy
            
            bpy.context.scene.frame_set(1)                                        

            assets_to_import = {asset_paths}
            for asset in assets_to_import:
                bpy.ops.gpco.command_read_asset(asset_source_scene = asset[1], asset_name = "INIT")
        
                search_string = asset[0]

                # Get all collections
                all_collections = bpy.data.collections
                print(all_collections)
                # Filter collections by name containing the search string
                for collection in all_collections:
                    if search_string in collection.name:
                        collectionToMove = collection
                        break
                    
                old_parent_collection = bpy.data.collections.get("COL_FIX_CHAR-NAME")
                parent_collection = bpy.data.collections.get("COL_ANIM")
                print(parent_collection)
                print(collectionToMove)
                if parent_collection and collectionToMove:
                    # Set the parent of the child collection
                    old_parent_collection.children.unlink(collectionToMove)
                    parent_collection.children.link(collectionToMove)
                        # args : asset_source_scene = the absolute path to the source scene cannot be None
                        # asset_name = the name of the asset to import cannot be None
                        # root_name = the name of the root object to import from. If None, the first root object of the asset will be used

            fileName = bpy.path.basename(bpy.context.blend_data.filepath)
            proj, ep, seq, sh, task, user = fileName.split("_")

            collection_name = "COL_BG_" + seq + "_" + sh + "_PAINT"
            print(collection_name)
            scene_path = "{bg_path}"
            print(scene_path)

            with bpy.data.libraries.load(scene_path, link=True) as (data_from, data_to):
                data_to.collections = [name for name in data_from.collections if collection_name in name]

            # Append the linked collection to the current scene
            if collection_name in bpy.data.collections:
                print("collection name found")
                bpy.context.scene.collection.children.link(bpy.data.collections[collection_name])
            else:
                print("Collection name not found")

            bpy.ops.wm.save_mainfile() 
            
        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path

    def run(self, button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()
            
            
            self.Log._log_view.append_text("Let's go !")
            script_path = self.write_script(self.shot_info)
            
            self.Log._log_view.append_text(f'Writting script in temp : {script_path}')
            self.Log._log_view.append_text(f'Build scene : "\n" {working_copy_path}')
            
            return GenericRunAction.run(self, button)
            # return super(AN_BuildScene, self).run(button)

class GPMAT_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    shot_info = []
    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""
    _source_scene = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset_lib = lib_oid.rsplit('/', 1)[-1]
        asset_type = type_oid.rsplit('/', 1)[-1]
        if file_type == "Works" and self._task.name() == "GPMAT" and asset_type == "CHARACTERS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        
        hasWorkingCopy = self._file.has_working_copy()
    
        if hasWorkingCopy:
            
            self.message.set("Build GPMAT scene ?") # Build the scene only if it has a working copy
            self._process_build = True
            return True

        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]
    
    def write_script(self):
        
        base_folder_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/"
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset_lib = lib_oid.rsplit('/', 1)[-1]
        asset_type = type_oid.rsplit('/', 1)[-1]
        asset = asset_oid.rsplit('/', 1)[-1]

        base_folder_path += (asset_lib + "/" + asset_type + "/" + asset + "/") 

        color_base_folder_path = base_folder_path + "05_COLOR/TO_PIPE" 
        
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        
        bpy.context.view_layer.objects.active = bpy.context.scene.objects[0]
        bpy.ops.object.mode_set(mode='OBJECT')

        color_base_folder_path = "{color_base_folder_path}"

        if not os.path.exists(color_base_folder_path):
            print(color_base_folder_path, " doesn't exist, skipping")
        else:

            # List all files in the current folder
            image_files = [f for f in os.listdir(color_base_folder_path) if f.lower().endswith(('.png', '.jpg'))]

            
            # Add images from the folder as reference images (Image as Planes)
            for image_file in image_files:
                # Set the image path for the reference image
                image_path = os.path.join(color_base_folder_path, image_file)

                # Load image as a reference image (Image as Plane)
                bpy.ops.object.load_reference_image(filepath=image_path)

                # Get the reference image object
                reference_image = bpy.context.object
                reference_image.name = image_file

                # scale the empty
                reference_image.empty_display_size = 30

                # Rotate the reference image by 90 degrees on the X-axis
                reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

                # unlink the object collection before moving it to the right one.
                bpy.context.collection.objects.unlink(reference_image)        


                Ref_collection = bpy.data.collections["COL_GPMAT_REFS"]
                Ref_collection.objects.link(reference_image)

                print("Loaded image as reference image: ", image_path)   

        bpy.ops.wm.save_mainfile()                                         

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path

    def run(self, button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()
            
            print("Let's go !")
            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(GPMAT_BuildScene, self).run(button)

class GPMAT_PROPS_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    shot_info = []
    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""
    _source_scene = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]
        asset_type = type_oid.rsplit('/', 1)[-1]
        if file_type == "Works" and self._task.name() == "GPMAT" and asset_type == "PROPS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        
        hasWorkingCopy = self._file.has_working_copy()
    
        if hasWorkingCopy:
            
            self.message.set("<h2>Build GPMAT scene ?</h2>") # Build the scene only if it has a working copy
            self._process_build = True
            return True

        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def get_source_scene(self):
        files = self._task.get_files( include_links=True, file_type="Inputs", primary_only=True)
        for file in files:
            if file.name() == ("MODEL_SHEET_PROPS_OUTPUT_blend"):
                src_file = file
                break
        HR_path = src_file.get_head_revision().get_path()
        print("src_file : ", HR_path)
        return HR_path
    
    def write_script(self):
        
        base_folder_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/"
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset_lib = lib_oid.rsplit('/', 1)[-1]
        asset_type = type_oid.rsplit('/', 1)[-1]
        asset = asset_oid.rsplit('/', 1)[-1]

        self._source_scene = self._source_scene.replace("/", "\\\\")

        base_folder_path += (asset_lib + "/" + asset_type + "/" + asset + "/") 

        color_base_folder_path = base_folder_path + "05_COLOR/TO_PIPE" 
        
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        
        bpy.context.view_layer.objects.active = bpy.context.scene.objects[0]
        bpy.ops.object.mode_set(mode='OBJECT')

        color_base_folder_path = "{color_base_folder_path}"

        if not os.path.exists(color_base_folder_path):
            print(color_base_folder_path, " doesn't exist, skipping")
        else:

            # List all files in the current folder
            image_files = [f for f in os.listdir(color_base_folder_path) if f.lower().endswith(('.png', '.jpg'))]

            
            # Add images from the folder as reference images (Image as Planes)
            for image_file in image_files:
                # Set the image path for the reference image
                image_path = os.path.join(color_base_folder_path, image_file)

                # Load image as a reference image (Image as Plane)
                bpy.ops.object.load_reference_image(filepath=image_path)

                # Get the reference image object
                reference_image = bpy.context.object
                reference_image.name = image_file

                # scale the empty
                reference_image.empty_display_size = 30

                # Rotate the reference image by 90 degrees on the X-axis
                reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

                # unlink the object collection before moving it to the right one.
                bpy.context.collection.objects.unlink(reference_image)        


                Ref_collection = bpy.data.collections["COL_GPMAT_REFS"]
                Ref_collection.objects.link(reference_image)

                print("Loaded image as reference image: ", image_path)   

        # Set the path to the blend file with the library
        library_path = '{self._source_scene}'

        # Load the objects from the library
        with bpy.data.libraries.load(library_path, link=True, relative=False) as (data_from, data_to):
            bpy.context.view_layer.update()
            # Check if the object exists in the library
            target_col_name = "COL_MS_{asset}"
            if target_col_name in data_from.collections: 
                data_to.collections.append(target_col_name)

        collection = bpy.data.collections["COL-GP"]
        ms = bpy.data.collections["COL_MS_{asset}"]
        collection.children.link(ms)

        bpy.ops.wm.save_mainfile()                                         

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path

    def run(self, button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()
            
            # Here get the model sheet task output file to import the collection from
            self._source_scene = self.get_source_scene().replace("\\", "/")
            print("Let's go !")
            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(GPMAT_BuildScene, self).run(button)

class PLT_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    shot_info = []
    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""
    _color_picker_images = []
    _source_scene = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]
        asset_type = type_oid.rsplit('/', 1)[-1]
        if file_type == "Works" and self._task.name() == "PALETTE" and asset_type == "CHARACTERS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def get_color_picker_images(self):
        base_folder_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/"
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset_lib = lib_oid.rsplit('/', 1)[-1]
        asset_type = type_oid.rsplit('/', 1)[-1]
        asset = asset_oid.rsplit('/', 1)[-1]

        base_folder_path += (asset_lib + "/" + asset_type + "/" + asset + "/") 

        color_base_folder_path = base_folder_path + "05_COLOR/TO_PIPE"
        # print(color_base_folder_path)

        if os.path.exists(color_base_folder_path) and os.path.isdir(color_base_folder_path):
            # List all files in the folder
            files = os.listdir(color_base_folder_path)

            # Filter images with extensions .png and .jpg
            image_files = [f for f in files if f.lower().endswith(('.png', '.jpg'))]

            # Check if the image name contains "COLOR_PICKER"
            self._color_picker_images = [os.path.join(color_base_folder_path, img).replace("\\", "/") for img in image_files if "COLOR_PICKER" in img.upper()]

            for image_path in self._color_picker_images:
                print(image_path)
            
        return self._color_picker_images

    def needs_dialog(self):
        
        hasWorkingCopy = self._file.has_working_copy()
    
        if hasWorkingCopy:
            
            # Need To check if the images for color picker exist before launching the thing
            oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
            lib_oid, type_oid, asset_oid = oids[-6:-3]

            asset_lib = lib_oid.rsplit('/', 1)[-1]
            
            if self.get_color_picker_images():

                self.message.set(f'<h2>Building Palette ?</h2>') # Build the scene only if it has a working copy
                self._process_build = True
                return True
            elif asset_lib == "LIBRARIES":
                self.message.set(f'<h2>No need for color picker image, build scene ?</h2>') # Build the scene only if it has a working copy
                self._process_build = True
                return True    
            else:
                self.message.set(f'<h2>No Color Picker image found or folder does not exist.</h2>') # Build the scene only if it has a working copy
                self._process_build = False
                return True
            
        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def write_script(self):
         

        self._source_scene = self._source_scene.replace("/", "\\\\")
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        # Set the path to the blend file with the library
        library_path = '{self._source_scene}'

        # Load the objects from the library
        with bpy.data.libraries.load(library_path, link=True, relative=False) as (data_from, data_to):
            bpy.context.view_layer.update()
            # Check if the object exists in the library
            target_object_name = "GP_MAT"
            if target_object_name in data_from.objects: 
                data_to.objects.append(target_object_name)

        collection = bpy.data.collections["COL_PLT"]
        GP = bpy.data.objects["GP_MAT"]
        collection.objects.link(GP)
        

        for image_file in {self._color_picker_images}:
            # Set the image path for the reference image

            # Load image as a reference image (Image as Plane)
            bpy.ops.object.load_reference_image(filepath=image_file)

            # Get the reference image object
            reference_image = bpy.context.object
            reference_image.name = os.path.splitext(os.path.basename(image_file))[0]

            # scale the empty
            reference_image.empty_display_size = 30

            # Rotate the reference image by 90 degrees on the X-axis
            reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

            # unlink the object collection before moving it to the right one.
            bpy.context.collection.objects.unlink(reference_image)        


            Ref_collection = bpy.data.collections["COL_IMG-FOR-PLT"]
            Ref_collection.objects.link(reference_image)

            print("Loaded image as reference image: ", image_file)   

        bpy.ops.wm.save_mainfile()                                         

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path

    def get_source_scene(self):
        files = self._task.get_files( include_links=True, file_type="Inputs", primary_only=True)
        for file in files:
            if file.name() == ("GPMAT_OUTPUT_blend"):
                src_file = file
                break
        HR_path = src_file.get_head_revision().get_path()
        print("src_file : ", HR_path)
        return HR_path

    def run(self, button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()
            
            self._source_scene = self.get_source_scene().replace("\\", "/")
            
            print("Let's go !")
            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(PLT_BuildScene, self).run(button)

class PLT_PROPS_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    shot_info = []
    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""
    _color_picker_images = []
    GPMAT_source_scene = ""
    MS_source_scene = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]
        asset_type = type_oid.rsplit('/', 1)[-1]
        if file_type == "Works" and self._task.name() == "PALETTE" and asset_type == "PROPS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def get_color_picker_images(self):
        base_folder_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/"
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset_lib = lib_oid.rsplit('/', 1)[-1]
        asset_type = type_oid.rsplit('/', 1)[-1]
        asset = asset_oid.rsplit('/', 1)[-1]

        base_folder_path += (asset_lib + "/" + asset_type + "/" + asset + "/") 

        color_base_folder_path = base_folder_path + "05_COLOR/TO_PIPE"
        # print(color_base_folder_path)

        if os.path.exists(color_base_folder_path) and os.path.isdir(color_base_folder_path):
            # List all files in the folder
            files = os.listdir(color_base_folder_path)

            # Filter images with extensions .png and .jpg
            image_files = [f for f in files if f.lower().endswith(('.png', '.jpg'))]

            # Check if the image name contains "COLOR_PICKER"
            self._color_picker_images = [os.path.join(color_base_folder_path, img).replace("\\", "/") for img in image_files if "COLOR_PICKER" in img.upper()]

            for image_path in self._color_picker_images:
                print(image_path)
            
        return self._color_picker_images


    def needs_dialog(self):
        
        hasWorkingCopy = self._file.has_working_copy()
    
        if hasWorkingCopy:
            
            # Need To check if the images for color picker exist before launching the thing
            oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
            lib_oid, type_oid, asset_oid = oids[-6:-3]

            asset_lib = lib_oid.rsplit('/', 1)[-1]
            self.get_color_picker_images()

            self.message.set(f'<h2>Building Palette ?</h2>') # Build the scene only if it has a working copy
            self._process_build = True
            return True
            
            
        else:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def write_script(self):
        
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]
        asset = asset_oid.rsplit('/', 1)[-1]

        self.GPMAT_source_scene = self.GPMAT_source_scene.replace("/", "\\\\")
        self.MS_source_scene = self.MS_source_scene.replace("/", "\\\\")
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        # Set the path to the blend file with the library
        GPMAT_library_path = '{self.GPMAT_source_scene}'

        # Load the objects from the library
        with bpy.data.libraries.load(GPMAT_library_path, link=True, relative=False) as (data_from, data_to):
            bpy.context.view_layer.update()
            # Check if the object exists in the library
            target_object_name = "GP_MAT"
            if target_object_name in data_from.objects: 
                data_to.objects.append(target_object_name)

        collection = bpy.data.collections["COL_PLT"]
        GP = bpy.data.objects["GP_MAT"]
        collection.objects.link(GP)
        
        # Set the path to the blend file with the library
        MS_library_path = '{self.MS_source_scene}'

        # Load the objects from the library
        with bpy.data.libraries.load(MS_library_path, link=True, relative=False) as (data_from, data_to):
            bpy.context.view_layer.update()
            # Check if the object exists in the library
            target_col_name = "COL_MS_{asset}"
            if target_col_name in data_from.collections: 
                data_to.collections.append(target_col_name)

        collection = bpy.data.collections["COL-GP"]
        ms = bpy.data.collections["COL_MS_{asset}"]
        collection.children.link(ms)


        for image_file in {self._color_picker_images}:
            # Set the image path for the reference image

            # Load image as a reference image (Image as Plane)
            bpy.ops.object.load_reference_image(filepath=image_file)

            # Get the reference image object
            reference_image = bpy.context.object
            reference_image.name = os.path.splitext(os.path.basename(image_file))[0]

            # scale the empty
            reference_image.empty_display_size = 30

            # Rotate the reference image by 90 degrees on the X-axis
            reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

            # unlink the object collection before moving it to the right one.
            bpy.context.collection.objects.unlink(reference_image)        


            Ref_collection = bpy.data.collections["COL_IMG-FOR-PLT"]
            Ref_collection.objects.link(reference_image)

            print("Loaded image as reference image: ", image_file)   

        bpy.ops.wm.save_mainfile()                                         

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path

    def get_GPMAT_source_scene(self):
        files = self._task.get_files( include_links=True, file_type="Inputs", primary_only=True)
        for file in files:
            if file.name() == ("GPMAT_OUTPUT_blend"):
                src_file = file
                break
        HR_path = src_file.get_head_revision().get_path()
        print("src_file : ", HR_path)
        return HR_path
    
    def get_MS_source_scene(self):
        files = self._task.get_files( include_links=True, file_type="Inputs", primary_only=True)
        for file in files:
            if file.name() == ("MODEL_SHEET_PROPS_OUTPUT_blend"):
                src_file = file
                break
        HR_path = src_file.get_head_revision().get_path()
        print("src_file : ", HR_path)
        return HR_path

    def run(self, button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()
            
            self.GPMAT_source_scene = self.get_GPMAT_source_scene().replace("\\", "/")
            self.MS_source_scene = self.get_MS_source_scene().replace("\\", "/")
            
            print("Let's go !")
            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(PLT_BuildScene, self).run(button)



class RIG_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    shot_info = []
    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""
    _color_picker_images = []
    _source_scene = ""
    _source_palette = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "RIG_2D_CHARS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        
        hasWorkingCopy = self._file.has_working_copy()
    
        if hasWorkingCopy:
            self.message.set(f'<h2>Building Rig 2D Scene ?</h2>') # Build the scene only if it has a working copy
            self._process_build = True
            return True
                      
        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def write_script(self):
         
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset = asset_oid.rsplit('/', 1)[-1]

        self._source_scene = self._source_scene.replace("/", "\\\\")
        self._source_palette = self._source_palette.replace("/", "\\\\")
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        # Set the path to the blend file with the library
        library_path = '{self._source_scene}'

        # Load the objects from the library
        with bpy.data.libraries.load(library_path, link=True, relative=False) as (data_from, data_to):
            bpy.context.view_layer.update()
            # Check if the object exists in the library
            target_col_name = "COL_MS_{asset}"
            if target_col_name in data_from.collections: 
                data_to.collections.append(target_col_name)

        collection = bpy.data.collections["COL-GP"]
        ms = bpy.data.collections["COL_MS_{asset}"]
        collection.children.link(ms)
                
        bpy.ops.scene.import_palette(filepath="{self._source_palette}")
           

        bpy.ops.wm.save_mainfile()                                         

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path

    def get_source_scene(self):
        files = self._task.get_files( include_links=True, file_type="Inputs", primary_only=True)
        for file in files:
            if file.name() == ("MODEL_SHEET_OUTPUT_blend"):
                src_file = file
                break
        HR_path = src_file.get_head_revision().get_path()
        print("src_file : ", HR_path)
        return HR_path
    
    def get_source_palette(self):
        files = self._task.get_files( include_links=True, file_type="Inputs", primary_only=True)
        for file in files:
            if file.name() == ("PALETTE_json"):
                src_file = file
                break
        HR_path = src_file.get_head_revision().get_path()
        print("src_file : ", HR_path)
        return HR_path

    def run(self, button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()
            
            self._source_scene = self.get_source_scene().replace("\\", "/")
            self._source_palette = self.get_source_palette().replace("\\", "/")

            
            print("Let's go !")
            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(RIG_BuildScene, self).run(button)

class RIG_PROPS_BuildScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    shot_info = []
    ICON = ("icons", "build")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _process_build = False
    _script_path = ""
    _color_picker_images = []
    _source_scene = ""
    _source_palette = ""

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "RIG_2D_PROPS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        
        hasWorkingCopy = self._file.has_working_copy()
    
        if hasWorkingCopy:
            self.message.set(f'<h2>Building Rig 2D Props ?</h2>') # Build the scene only if it has a working copy
            self._process_build = True
            return True
                      
        elif hasWorkingCopy == False:    
            self.message.set(f'<h2>You need to create a working copy before building the scene</h2>')
            self._process_build = False
            return True
    
    def get_buttons(self):
        if self._process_build:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        wc = self._file.get_working_copy()
        return [ wc.get_path(),'--background', '--python', self._script_path]

    def write_script(self):
         
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]

        asset = asset_oid.rsplit('/', 1)[-1]
        print("Asset to import - ", asset)

        self._source_scene = self._source_scene.replace("/", "\\\\")
        self._source_palette = self._source_palette.replace("/", "\\\\")
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        # Set the path to the blend file with the library
        library_path = '{self._source_scene}'

        # Load the objects from the library
        with bpy.data.libraries.load(library_path, link=True, relative=False) as (data_from, data_to):
            bpy.context.view_layer.update()
            # Check if the object exists in the library
            target_col_name = "COL_MS_{asset}"
            if target_col_name in data_from.collections: 
                data_to.collections.append(target_col_name)

        collection = bpy.data.collections["COL_REFS"]
        print(collection)
        ms = bpy.data.collections["COL_MS_{asset}"]
        print(ms)
        collection.children.link(ms)

        try:        
            bpy.ops.scene.import_palette(filepath="{self._source_palette}")
        except:
            print("Cannot import palette, weirdo !")  

        bpy.ops.wm.save_mainfile()                                         

        ''')

        temp_folder = tempfile.gettempdir()
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        return build_scene_script_path

    def get_source_scene(self):
        files = self._task.get_files( include_links=True, file_type="Inputs", primary_only=True)
        for file in files:
            if file.name() == ("MODEL_SHEET_PROPS_OUTPUT_blend"):
                src_file = file
                break
        HR_path = src_file.get_head_revision().get_path()
        print("src_file : ", HR_path)
        return HR_path
    
    def get_source_palette(self):
        files = self._task.get_files( include_links=True, file_type="Inputs", primary_only=True)
        for file in files:
            if file.name() == ("PALETTE_json"):
                src_file = file
                break
        HR_path = src_file.get_head_revision().get_path()
        print("src_file : ", HR_path)
        return HR_path

    def run(self, button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            working_copy = self._file.get_working_copy()
            working_copy_path = working_copy.get_path()
            
            self._source_scene = self.get_source_scene().replace("\\", "/")
            self._source_palette = self.get_source_palette().replace("\\", "/")

            
            print("Let's go !")
            script_path = self.write_script()

            print("Writting script in temp : ", script_path)
            print("Build scene : ", working_copy_path)
            return GenericRunAction.run(self, button)
            # return super(RIG_BuildScene, self).run(button)






def RIG_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(RIG_BuildScene) # Get the relation
        relation.name = 'Build_Scene_RIG' # Explicitely definining its name
        return relation # Return relation

def RIG_PROPS_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(RIG_PROPS_BuildScene) # Get the relation
        relation.name = 'Build_Scene_RIG_Props' # Explicitely definining its name
        return relation # Return relation

def PLT_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(PLT_BuildScene) # Get the relation
        relation.name = 'Build_Scene_PLT' # Explicitely definining its name
        return relation # Return relation

def PLT_PROPS_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(PLT_PROPS_BuildScene) # Get the relation
        relation.name = 'Build_Scene_PLT_Props' # Explicitely definining its name
        return relation # Return relation

def GPMAT_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(GPMAT_BuildScene) # Get the relation
        relation.name = 'Build_Scene_GPMAT' # Explicitely definining its name
        return relation # Return relation

def GPMAT_PROPS_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(GPMAT_PROPS_BuildScene) # Get the relation
        relation.name = 'Build_Scene_GPMAT_Props' # Explicitely definining its name
        return relation # Return relation

def AN_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(AN_BuildScene) # Get the relation
        relation.name = 'Build_Scene_ANIM' # Explicitely definining its name
        return relation # Return relation

def BG_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(BG_BuildScene) # Get the relation
        relation.name = 'Build_Scene_BG' # Explicitely definining its name
        return relation # Return relation

def MS_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(MS_BuildScene) # Get the relation
        relation.name = 'Build_Scene_MS' # Explicitely definining its name
        return relation # Return relation

def MS_PROPS_build_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(MS_PROPS_BuildScene) # Get the relation
        relation.name = 'Build_Scene_MS_PROPS' # Explicitely definining its name
        return relation # Return relation

def install_extensions(session):
    return {
        'build_scene_action': [
            MS_build_scene,
            BG_build_scene,
            AN_build_scene,
            GPMAT_build_scene,
            PLT_build_scene,
            PLT_PROPS_build_scene,
            RIG_build_scene,
            RIG_PROPS_build_scene,
            MS_PROPS_build_scene,
            GPMAT_PROPS_build_scene
        ]
    }

