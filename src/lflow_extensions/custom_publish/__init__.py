import os
import tempfile
import textwrap
from kabaret import flow
from libreflow.baseflow.file import File, OpenWithAction
import shutil


class CustomPublishPSD(flow.Action):



    ICON = ("icons.flow", "photoshop")
    _process_custom_publish = False
    _file = flow.Parent()
    _task = flow.Parent(3)

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "PAINT_BG": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        hasWorkingCopy = self._file.has_working_copy()
        if hasWorkingCopy:

            self.message.set(f'<h2>Publish And Generate Assets ? ?</h2>') # Build the scene only if it has a working copy
            self._process_custom_publish = True
            return True

        else :    
            self.message.set(f'<h2>You need to have a working copy to publish...</h2>')
            self._process_custom_publish = False
            return True
    
    def get_buttons(self):
        if self._process_custom_publish:
            return ['Cancel', 'Ok']
        else:
            return['Got it']

    def publishAndRenameAssetFolder(self):

        WC_path = self._file.get_working_copy().get_path().replace("\\", "/")
        print("WC_path : ", WC_path)
        # creating the file format to publish and rename the asset folder
        no_extension = WC_path.split(".")[0]
        # print("no_extension : ", no_extension)
        file_name = no_extension.split("/")[-1]
        # print("file_name : ", file_name)
        base_name = file_name.rsplit("_", 1)[0]
        # print("base_name : ", base_name)
        asset_folder_path = no_extension+"-assets"
        if os.path.exists(asset_folder_path):
            # print("Folder exists !")

            # Now we need to publish the WC
            self._file.publish(revision_name=None, source_path=None, comment=('Custom Published'), keep_editing=False, ready_for_sync=True, path_format=None)
            version = self._file.get_head_revision().name()
            new_folder_name = base_name + "_" + version + "-assets"
            new_folder_path = os.path.join(os.path.dirname(asset_folder_path), new_folder_name)
            os.rename(asset_folder_path, new_folder_path)
            return True

        else:
            print("Can't find the folder")
            return

    def run(self,button):

        if button == 'Cancel':
            print("Cancelled")
            return
        
        if button == 'Got it':
            print("Cancelled")
            return
        
        if button == 'Ok':
            print("Publishing !")
            self.publishAndRenameAssetFolder()

class CustomPublishLayout(flow.Action):


    ICON = ("icons", "custom_publish_layout")
    _process_custom_publish = False
    _file = flow.Parent()
    _task = flow.Parent(3)

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "LAYOUT_BG": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        hasWorkingCopy = self._file.has_working_copy()
        if hasWorkingCopy:

            self.message.set(f'<h2>Publish along with GP Renders ?</h2> <h3>This takes a bit of time...</h3>') # Build the scene only if it has a working copy
            self._process_custom_publish = True
            return True

        else :    
            self.message.set(f'<h2>You need to have a working copy to publish...</h2>')
            self._process_custom_publish = False
            return True
    
    def get_buttons(self):
        if self._process_custom_publish:
            return ['Cancel', 'Ok']
        else:
            return['Got it']

    def publishAndCopyGpRenders(self):

        WC_path = self._file.get_working_copy().get_path().replace("\\", "/")
        print("WC_path : ", WC_path)
        # creating the file format to publish and rename the asset folder
        
        source_dir, file_name = os.path.split(WC_path)
        print("directory : ", source_dir)
        
        self._file.publish(revision_name=None, source_path=None, comment=('Custom Published'), keep_editing=False, ready_for_sync=True, path_format=None)
        contents = os.listdir(source_dir)
        print("contents : ", contents)

        HR_path = self._file.get_head_revision().get_path().replace("\\", "/")
        dest_dir, file_name = os.path.split(HR_path)
        print("dest_dir : ", dest_dir)
        
        for item in contents:
            source_item = os.path.join(source_dir, item)
            print("source_item : ", source_item)
            destination_item = os.path.join(dest_dir, item)
            print("destination_item : ", destination_item)
            if os.path.isdir(source_item):
                # If it's a directory, use shutil.copytree to copy the entire directory
                
                shutil.copytree(source_item, destination_item)
                
            else:
                # If it's a file, use shutil.copy2 to copy the file, preserving metadata
                shutil.copy2(source_item, destination_item)
                base, extension = os.path.splitext(destination_item)
                if extension.lower() == ".json":
                    # Let's rename the json so it matches the revision name
                    directory, filename = os.path.split(destination_item)
                    HR = self._file.get_head_revision()
                    file_name = HR.get_path().split("/")[-1]
                    base_name = file_name.split(".")[0]
                    new_filename = base_name+".json"
                    new_file_path = os.path.join(directory, new_filename)
                    print("About to rename : ", destination_item, " to : ", new_file_path)
                    os.rename(destination_item, new_file_path)

    def run(self,button):

        if button == 'Cancel':
            print("Cancelled")
            return
        
        if button == 'Got it':
            print("Cancelled")
            return
        
        if button == 'Ok':
            print("Publishing !")
            self.publishAndCopyGpRenders()
         
class CustomPublishRig(flow.Action):


    ICON = ("icons", "custom_publish_rig")
    _process_custom_publish = False
    _file = flow.Parent()
    _task = flow.Parent(3)
    Log = None

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "RIG_2D_CHARS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        hasWorkingCopy = self._file.has_working_copy()
        if hasWorkingCopy:

            self.message.set(f'<h2>Publish along with Gifs ?</h2> <h3>This can take a bit of time according to the number of gifs...</h3>') # Build the scene only if it has a working copy
            self._process_custom_publish = True
            return True

        else :    
            self.message.set(f'<h2>You need to have a working copy to publish...</h2>')
            self._process_custom_publish = False
            return True
    
    def get_buttons(self):
        if self._process_custom_publish:
            return ['Cancel', 'Ok']
        else:
            return['Got it']

    def copy_gif_files(self,source_dir, destination_dir):
        # Scan the source directory
        for filename in os.listdir(source_dir):
            source_file_path = os.path.join(source_dir, filename)

            # Check if the file is a .gif file
            if (filename.lower().endswith('.gif') or filename.lower().endswith('.txt')) and os.path.isfile(source_file_path):
                destination_file_path = os.path.join(destination_dir, filename)

                # Copy the file to the destination directory
                shutil.copy2(source_file_path, destination_file_path)
                self.Log._log_view.append_text(f"Copied {filename} to {destination_dir}")

    def publishAndCopyGifs(self):

        WC_path = self._file.get_working_copy().get_path().replace("\\", "/")
        self.Log._log_view.append_text("WC_path : "+ WC_path)
        # creating the file format to publish and rename the asset folder
        
        source_dir, file_name = os.path.split(WC_path)
        self.Log._log_view.append_text("directory : "+ source_dir)
        
        self._file.publish(revision_name=None, source_path=None, comment=('Custom Published'), keep_editing=False, ready_for_sync=True, path_format=None)
        
        HR_path = self._file.get_head_revision().get_path().replace("\\", "/")
        dest_dir, file_name = os.path.split(HR_path)
        self.Log._log_view.append_text("dest_dir : "+ dest_dir)
        
        self.copy_gif_files(source_dir, dest_dir)

    def run(self,button):

        if button == 'Cancel':
            print("Cancelled")
            return
        
        if button == 'Got it':
            print("Cancelled")
            return
        
        if button == 'Ok':
            print("Publishing !")
            self.publishAndCopyGifs()
 

def custom_publish_rig(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(CustomPublishRig) # Get the relation
        relation.name = 'Publish_Rig_Along_with_Gifs' # Explicitely definining its name
        return relation # Return relation

def custom_publish_layout(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(CustomPublishLayout) # Get the relation
        relation.name = 'Publish_Layout_And_GP_Renders' # Explicitely definining its name
        return relation # Return relation

def custom_publish_psd(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(CustomPublishPSD) # Get the relation
        relation.name = 'Publish_PSD_And_Generate_Assets' # Explicitely definining its name
        return relation # Return relation

def install_extensions(session):
    return {
        'publish_action_action': [
            custom_publish_psd,
            custom_publish_layout,
            custom_publish_rig
        ]
    }
