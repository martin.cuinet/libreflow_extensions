
from kabaret import flow
from libreflow.baseflow.asset import Asset, AssetType, AssetLibrary



class DeleteCurrentAsset(flow.Action):

    ICON = ("icons", "delete")
    _asset_type = flow.Parent(2)
    _asset = flow.Parent()

    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        if userType == "Admin":
            return True
        else:
            return False

    def needs_dialog(self):
        self.message.set(f'<h2>Are you sure you want to delete this Asset ?</h2>')
        return True
    

    def get_buttons(self):
        return ['Cancel', 'Ok']
    

    def run(self,button):

        if button == 'Cancel':
            return

        # S'il y a des taches, on les supprime
        for task in self._asset.tasks.mapped_items():
            # S'il y a des fichiers, on les supprime
            if len(task.files.mapped_items()) > 1:
                for f in task.files.mapped_items():
                    f.history.revisions.clear() # Supprime les revisions
                    f.current_revision.set('') # Reset la revision courante
                    f.last_revision_oid.set(None) # Reset le chemin de la derniere revision
                    
                    task.files.remove(f.name()) # Supprime l'objet correspondant au fichier
            
            self._asset.tasks.remove(task.name()) # Supprime la tache

        self._asset_type.remove(self._asset.name()) # Supprime l'asset  

class DeleteCurrentAssetType(flow.Action):

    ICON = ("icons", "delete")
    _asset_lib = flow.Parent(2)
    _asset_type = flow.Parent()

    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        if userType == "Admin":
            return True
        else:
            return False

    def needs_dialog(self):
        self.message.set(f'<h2>Are you sure you want to delete this Asset ?</h2>')
        return True
    

    def get_buttons(self):
        return ['Cancel', 'Ok']
    

    def run(self,button):

        if button == 'Cancel':
            return

        # S'il y a des assets, on les supprime
        for asset in self._asset_type.assets.mapped_items():
            # S'il y a des taches, on les supprime
            for task in asset.tasks.mapped_items():
                # S'il y a des fichiers, on les supprime
                if len(task.files.mapped_items()) > 1:
                    for f in task.files.mapped_items():
                        f.history.revisions.clear() # Supprime les revisions
                        f.current_revision.set('') # Reset la revision courante
                        f.last_revision_oid.set(None) # Reset le chemin de la derniere revision
                        
                        task.files.remove(f.name()) # Supprime l'objet correspondant au fichier
                
                asset.tasks.remove(task.name()) # Supprime la tache

            self._asset_type.assets.remove(asset.name()) # Supprime l'asset

        self._asset_lib.remove(self._asset_type.name()) # Supprime l'asset type 

class DeleteCurrentAssetLib(flow.Action):

    ICON = ("icons", "delete")
    _project = flow.Parent(2)
    _asset_lib = flow.Parent()

    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        if userType == "Admin":
            return True
        else:
            return False

    def needs_dialog(self):
        self.message.set(f'<h2>Are you sure you want to delete this Asset ?</h2>')
        return True
    

    def get_buttons(self):
        return ['Cancel', 'Ok']
    

    def run(self,button):

        if button == 'Cancel':
            return

        # S'il y a des asset types, on les supprime
        for asset_type in self._asset_lib.asset_types.mapped_items():
            # S'il y a des assets, on les supprime
            for asset in asset_type.assets.mapped_items():
                # S'il y a des taches, on les supprime
                for task in asset.tasks.mapped_items():
                    # S'il y a des fichiers, on les supprime
                    if len(task.files.mapped_items()) > 1:
                        for f in task.files.mapped_items():
                            f.history.revisions.clear() # Supprime les revisions
                            f.current_revision.set('') # Reset la revision courante
                            f.last_revision_oid.set(None) # Reset le chemin de la derniere revision
                            
                            task.files.remove(f.name()) # Supprime l'objet correspondant au fichier
                    
                    asset.tasks.remove(task.name()) # Supprime la tache

                asset_type.assets.remove(asset.name()) # Supprime l'asset
            
            self._asset_lib.asset_types.remove(asset_type.name()) # Supprime l'asset type

        self._project.remove(self._asset_lib.name()) # Supprime l'asset lib 




def delete_asset(parent):
    
    if isinstance(parent, Asset): # Check if the parent is of type Task
        relation = flow.Child(DeleteCurrentAsset).ui(hidden=True) # Get the relation
        relation.name = 'delete_asset' # Explicitely definining its name
        return relation # Return relation
    
def delete_asset_type(parent):
    
    if isinstance(parent, AssetType): # Check if the parent is of type Task
        relation = flow.Child(DeleteCurrentAssetType).ui(hidden=True) # Get the relation
        relation.name = 'delete_asset_type' # Explicitely definining its name
        return relation # Return relation
    
def delete_asset_lib(parent):
    
    if isinstance(parent, AssetLibrary): # Check if the parent is of type Task
        relation = flow.Child(DeleteCurrentAssetLib).ui(hidden=True) # Get the relation
        relation.name = 'delete_asset_lib' # Explicitely definining its name
        return relation # Return relation



def install_extensions(session):
    return {
        'Assets_action': [
            delete_asset,
            delete_asset_type,
            delete_asset_lib
        ]
    }
