
from kabaret import flow
from libreflow.baseflow.task import Task
from libreflow.baseflow.file import File, FileSystemRefCollection


class ClearCurrentTask(flow.Action):

    _task = flow.Parent()
    ICON = ("icons", "delete")
    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        if userType == "Admin":
            return True
        else:
            return False

    def needs_dialog(self):
        self.message.set(f'<h2>Are you sure you want to delete these files ?</h2>')
        return True

    def get_buttons(self):
        return ['Cancel', 'Ok']

    def run(self,button):

        if button == 'Cancel':
            return
        
        for f in self._task.files.mapped_items():
            f.history.revisions.clear() # Supprime les revisions
            f.current_revision.set('') # Reset la revision courante
            f.last_revision_oid.set(None) # Reset le chemin de la derniere revision
            
            self._task.files.remove(f.name()) # Supprime l'objet correspondant au fichier   
             
        self._task.touch()


def clear_task(parent):
    
    if isinstance(parent, Task): # Check if the parent is of type Task
        relation = flow.Child(ClearCurrentTask) # Get the relation
        relation.name = 'Clear_Current_Task' # Explicitely definining its name
        return relation # Return relation

def install_extensions(session):
    return {
        'task_actions': [
            clear_task
        ]
    }
