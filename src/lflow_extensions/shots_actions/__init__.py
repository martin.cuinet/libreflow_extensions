
from kabaret import flow
from libreflow.baseflow.shot import Shot, Sequence


class DeleteCurrentShot(flow.Action):

    ICON = ("icons", "delete")
    _sequence = flow.Parent(2)
    _shot = flow.Parent()

    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        if userType == "Admin":
            return True
        else:
            return False

    def needs_dialog(self):
        self.message.set(f'<h2>Are you sure you want to delete this Shot ?</h2>')
        return True
    

    def get_buttons(self):
        return ['Cancel', 'Ok']
    

    def run(self,button):

        if button == 'Cancel':
            return

        shot = self._shot
        sequence = self._sequence

        # S'il y a des taches, on les supprime
        for task in shot.tasks.mapped_items():
            # S'il y a des fichiers, on les supprime
            if len(task.files.mapped_items()) > 1:
                for f in task.files.mapped_items():
                    f.history.revisions.clear() # Supprime les revisions
                    f.current_revision.set('') # Reset la revision courante
                    f.last_revision_oid.set(None) # Reset le chemin de la derniere revision
                    
                    task.files.remove(f.name()) # Supprime l'objet correspondant au fichier
            
            shot.tasks.remove(task.name()) # Supprime la tache

        sequence.remove(shot.name()) # Supprime le shot  
        sequence.touch()

class DeleteCurrentSequence(flow.Action):

    ICON = ("icons", "delete")
    _sequence_collection = flow.Parent(2)
    _sequence = flow.Parent()
    

    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        if userType == "Admin":
            return True
        else:
            return False

    def needs_dialog(self):
        self.message.set(f'<h2>Are you sure you want to delete this Sequence ?</h2>')
        return True
    

    def get_buttons(self):
        return ['Cancel', 'Ok']
    

    def run(self,button):

        if button == 'Cancel':
            return

        sequence_collection = self._sequence_collection
        sequence = self._sequence

        # S'il y a des shots, on les supprime
        for shot in sequence.shots.mapped_items():
            # S'il y a des taches, on les supprime
            for task in shot.tasks.mapped_items():
                # S'il y a des fichiers, on les supprime
                if len(task.files.mapped_items()) > 1:
                    for f in task.files.mapped_items():
                        f.history.revisions.clear() # Supprime les revisions
                        f.current_revision.set('') # Reset la revision courante
                        f.last_revision_oid.set(None) # Reset le chemin de la derniere revision
                        
                        task.files.remove(f.name()) # Supprime l'objet correspondant au fichier
                
                shot.tasks.remove(task.name()) # Supprime la tache

            sequence.shots.remove(shot.name()) # Supprime le shot

        sequence_collection.remove(sequence.name()) # Supprime la sequence
        sequence_collection.touch()



def delete_shot(parent):
    
    if isinstance(parent, Shot): # Check if the parent is of type Task
        relation = flow.Child(DeleteCurrentShot).ui(hidden=True) # Get the relation
        relation.name = 'delete_shot' # Explicitely definining its name
        return relation # Return relation

def delete_sequence(parent):
    
    if isinstance(parent, Sequence): # Check if the parent is of type Task
        relation = flow.Child(DeleteCurrentSequence).ui(hidden=True) # Get the relation
        relation.name = 'delete_sequence' # Explicitely definining its name
        return relation # Return relation

def install_extensions(session):
    return {
        'Shots_action': [
            delete_shot,
            delete_sequence
        ]
    }
