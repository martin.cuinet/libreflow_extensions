from kabaret import flow
import os
import tempfile
from libreflow.baseflow.file import File, Revision, OpenWithAction, GenericRunAction
import re
import textwrap



class ClearCurrentFile(flow.Action):

    _file = flow.Parent()
    _task = flow.Parent(3)
    ICON = ("icons", "delete")
    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        if userType == "Admin":
            return True
        else:
            return False

    def needs_dialog(self):
        self.message.set(f'<h2>Are you sure you want to delete this file along with its revisions ?</h2>')
        return True

    def get_buttons(self):
        return ['Cancel', 'Ok']
    

    def run(self,button):

        if button == 'Cancel':
            return

        self._file.history.revisions.clear()
        self._file.current_revision.set('')
        self._file.last_revision_oid.set(None)
        self._task.files.remove(self._file.name())
        
class ClearCurrentRevision(flow.Action):

    revision = flow.Parent()
    _history = flow.Parent(3)
    _file = flow.Parent(4)

    ICON = ("icons", "delete")
    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        if userType == "Admin" or userType == "Supervisor":
            # need to check if the selected revision is not a published revision
            pattern = re.compile(r'^v\d{3}$')
            check = bool(pattern.match(self.revision.name()))
            if not check:
                return True
            else:
                return False
        else:
            return False

    def needs_dialog(self):
        self.message.set(f'<h2>Are you sure you want to delete this working copy ?</h2>')
        return True

    def get_buttons(self):
        return ['Cancel', 'Ok']
    

    def run(self,button):

        if button == 'Cancel':
            return
        
        rev = self._file.history.revisions[self.revision.name()]
        self._file.history.revisions.remove(rev.name()) # Supprime la revision
        
        file = self.revision._file
        file.get_revisions().touch()
        file.touch()

class OpenLoadUi(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    
    # ICON = ("icons", "bake")
    _file = flow.Parent()
    _file_system = flow.Parent(2)
    _task = flow.Parent(3)
    _script_path = ""
    process_open = False
    

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        user = self.root().project().get_user()
        userType = user.status.get()
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and (userType == "Admin" or userType == "Supervisor"):
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasRevision != None and hasWorkingCopy == True:

            self.message.set(f'<h2>Open with Load Ui ?</h2>') # Build the scene only if it has a working copy
            
            self.process_open = True
            return True

        elif hasWorkingCopy == False:    
            # self.render_type.ui(hidden=True)
            self.message.set(f'<h2>You need a working copy to open</h2>')
            self.process_open = False
            return True
    
    def get_buttons(self):
        if self.process_open:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        
        return ['--python', self._script_path]

    def write_script(self):

        currentFile = self._file.get_working_copy().get_path()
        render_scene_script_content = textwrap.dedent(f'''\
        import bpy
        bpy.ops.wm.open_mainfile(filepath="\\{currentFile}", load_ui = True)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'openScene-load_ui.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(render_scene_script_content)

        # print(f"Render script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            script_path = self.write_script()
            self.Log._log_view.append_text("Writting script in temp : " +  script_path)
            
            return GenericRunAction.run(self, button)
            # return super(renderScene, self).run(button)
 





def open_load_ui(parent):
    
    if isinstance(parent, File): # Check if the parent is of type Task
        relation = flow.Child(OpenLoadUi) # Get the relation
        relation.name = 'Open_And_Load_Ui' # Explicitely definining its name
        return relation # Return relation

def delete_file(parent):
    
    if isinstance(parent, File): # Check if the parent is of type Task
        relation = flow.Child(ClearCurrentFile) # Get the relation
        relation.name = 'clear_current_file' # Explicitely definining its name
        return relation # Return relation
    
def delete_revision(parent):
    
    if isinstance(parent, Revision): # Check if the parent is of type Task
        relation = flow.Child(ClearCurrentRevision) # Get the relation
        relation.name = 'delete_working_copy' # Explicitely definining its name
        return relation # Return relation



def install_extensions(session):
    return {
        'delete_file_action': [
            delete_file,
            delete_revision,
            open_load_ui
        ]
    }
