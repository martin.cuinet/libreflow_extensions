import os
from kabaret.app.ui.gui.widgets.flow.flow_view import (
    CustomPageWidget,
    QtWidgets,
    QtCore,
    QtGui,
)


class MyGreatActionWidget(CustomPageWidget):

    def build(self):
        lo = QtWidgets.QVBoxLayout(self)

        label = QtWidgets.QLabel()
        gif = QtGui.QMovie(f"{os.path.dirname(__file__)}\\rickroll.gif")
        label.setMovie(gif)
        
        lo.addWidget(label)
        gif.start()

    def sizeHint(self):
        return QtCore.QSize(600, 600)
