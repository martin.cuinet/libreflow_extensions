
from kabaret import flow
from libreflow.baseflow.shot import Sequence



class MySuperAction(flow.Action):
    
    def _fill_ui(self, ui):
        ui['custom_page'] = 'lflow_extensions.my_super_action.ui.my_great_action.MyGreatActionWidget'


def create_super_action(parent):
    if isinstance(parent, Sequence):
        relation = flow.Child(MySuperAction)
        relation.name = 'my_super_action'
        return relation


def install_extensions(session):
    return {
        'my_super_extension': [
            create_super_action
        ]
    }
