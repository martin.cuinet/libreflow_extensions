import os
import tempfile
import textwrap
from kabaret import flow
from libreflow.baseflow.file import File, OpenWithAction, GenericRunAction
from libreflow.baseflow.kitsu import KitsuAPIWrapper as kitsu
import gazu
import re

class MS_RenderChoiceValue(flow.values.ChoiceValue):

    _choices = ['-', 'TURN', 'HEAD', 'EXPRESSIONS', 'LIPS', 'HANDS']

    def choices(self):
        # print(self._choices)
        return self._choices

    def revert_to_default(self):     
        choices = self.choices()
        if choices:
            choice = choices[0]
        
        self.set(choice)
    
    def _fill_ui(self, ui):
        super(MS_RenderChoiceValue, self)._fill_ui(ui)

class AN_RenderChoiceValue(flow.values.ChoiceValue):

    _choices = ['FULL_RES', 'HALF_RES', 'QUARTER_RES']

    def choices(self):
        # print(self._choices)
        return self._choices

    def revert_to_default(self):     
        choices = self.choices()
        if choices:
            choice = choices[0]
        
        self.set(choice)
    
    def _fill_ui(self, ui):
        super(AN_RenderChoiceValue, self)._fill_ui(ui)

class MS_renderScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    
    render_part = flow.Param(None, MS_RenderChoiceValue).ui(label='Render Part :')
    ICON = ("icons", "bake")
    _file = flow.Parent()
    _file_system = flow.Parent(2)
    _task = flow.Parent(3)
    _script_path = ""
    _process_render = False
    _to_render = ""
    _start_frame = 0
    _stop_frame = 0
    _dest_file_path = ""
    Log = None

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")
        

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "MODEL_SHEET": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.render_part.revert_to_default()
        self.Log._log_view.clear()
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasRevision != None and hasWorkingCopy == False:

            self.message.set(f'<h2>Render this file ?</h2>') # Build the scene only if it has a working copy
            
            self._process_render = True
            return True

        elif hasWorkingCopy:    
            # self.render_part.ui(hidden=True)
            self.message.set(f'<h2>You need to publish the file before building the scene</h2>')
            self._process_render = False
            return True
    
    def get_buttons(self):
        if self._process_render:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]

    def get_dest_file(self,part):
        
        print("Part to render : ", part)
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            # print(file.name())
            if file.name() == (part+"_mp4"):
                dest_file = file
                print(dest_file)
                break
        if dest_file.has_working_copy() == False:
            print("create working Copy !")
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()
        # print(self._dest_file_path) 

        return self._dest_file_path



    def write_script(self):


        if self._to_render == 'TURN':
            self._start_frame = 1
            self._stop_frame = 39
        if self._to_render == 'HEAD':
            self._start_frame = 40
            self._stop_frame = 69
        if self._to_render == 'EXPRESSIONS':
            self._start_frame = 70
            self._stop_frame = 129
        if self._to_render == 'LIPS':
            self._start_frame = 130
            self._stop_frame = 219
        if self._to_render == 'HANDS':
            self._start_frame = 220
            self._stop_frame = 250

        

        render_scene_script_content = textwrap.dedent(f'''\
        import bpy

        # Set the start and end frames for rendering
        start_frame = {self._start_frame}
        end_frame = {self._stop_frame}

        # Set the output file path 
        output_file_path = "\\{self._dest_file_path}"

        # Set the rendering settings
        bpy.context.scene.frame_start = start_frame
        bpy.context.scene.frame_end = end_frame
        bpy.context.scene.render.filepath = output_file_path

        # Set the output format and codec options (modify as needed)
        bpy.context.scene.render.image_settings.file_format = 'FFMPEG'
        bpy.context.scene.render.ffmpeg.format = 'MPEG4'
        bpy.context.scene.render.ffmpeg.codec = 'H264'
        bpy.context.scene.render.ffmpeg.audio_codec = 'AAC'

        # Render the animation
        bpy.ops.render.render(animation=True)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'render_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(render_scene_script_content)

        # print(f"Render script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            HR = self._file.get_head_revision().get_path()
            

            self._to_render = self.render_part.get()
            self._dest_file_path = self.get_dest_file(self._to_render)
            if self._dest_file_path != "":
                script_path = self.write_script()
                print("Rendering ",self.render_part.get())
                print("Writting script in temp : ", script_path)
                print("Render scene : ", HR)
                return GenericRunAction.run(self, button)
                # return super(renderScene, self).run(button)
            else:
                print("Cannot find destination file to render to...")

class MS_PROPS_renderScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    
    ICON = ("icons", "bake")
    _file = flow.Parent()
    _file_system = flow.Parent(2)
    _task = flow.Parent(3)
    _script_path = ""
    _process_render = False
    _to_render = ""
    _start_frame = 0
    _stop_frame = 0
    _dest_file_path = ""
    Log = None

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "MODEL_SHEET_PROPS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasRevision != None and hasWorkingCopy == False:

            self.message.set(f'<h2>Render this file ?</h2>') # Build the scene only if it has a working copy
            
            self._process_render = True
            return True

        elif hasWorkingCopy:    
            
            self.message.set(f'<h2>You need to publish the file before building the scene</h2>')
            self._process_render = False
            return True
    
    def get_buttons(self):
        if self._process_render:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]

    def get_dest_file(self,part):
        
        print("file to render : ", part)
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            # print(file.name())
            if file.name() == (part+"_mp4"):
                dest_file = file
                print(dest_file)
                break
        if dest_file.has_working_copy() == False:
            print("create working Copy !")
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()
        # print(self._dest_file_path) 

        return self._dest_file_path



    def write_script(self):

        render_scene_script_content = textwrap.dedent(f'''\
        import bpy

        # Set the rendering settings

        # Set the output file path 
        output_file_path = "\\{self._dest_file_path}"

        # Set the rendering settings
        bpy.context.scene.frame_start = 1
        bpy.context.scene.frame_end = 50
        bpy.context.scene.render.filepath = output_file_path

        # Set the output format and codec options (modify as needed)
        bpy.context.scene.render.image_settings.file_format = 'FFMPEG'
        bpy.context.scene.render.ffmpeg.format = 'MPEG4'
        bpy.context.scene.render.ffmpeg.codec = 'H264'
        bpy.context.scene.render.ffmpeg.audio_codec = 'AAC'

        # Render the animation
        bpy.ops.render.render(animation=True)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'render_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(render_scene_script_content)

        # print(f"Render script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            HR = self._file.get_head_revision().get_path()
            

            
            self._dest_file_path = self.get_dest_file("TURN")
            if self._dest_file_path != "":
                script_path = self.write_script()
                print("Rendering TURN")
                print("Writting script in temp : ", script_path)
                print("Render scene : ", HR)
                return GenericRunAction.run(self, button)
                # return super(renderScene, self).run(button)
            else:
                print("Cannot find destination file to render to...")

class AN_renderScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    
    render_type = flow.Param(None, AN_RenderChoiceValue).ui(label='Render Size :')
    ICON = ("icons", "bake")
    _file = flow.Parent()
    _file_system = flow.Parent(2)
    _task = flow.Parent(3)
    _script_path = ""
    _process_render = False
    _to_render = ""
    _start_frame = 0
    _stop_frame = 0
    _dest_file_path = ""
    Log = None

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and (self._task.name() == "LAYOUT_POSING" or self._task.name() == "BLOCKING" or self._task.name() == "ANIMATION" or self._task.name() == "CLEAN" or self._task.name() == "FX"): # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasRevision != None and hasWorkingCopy == False:

            self.message.set(f'<h2>Render this file ?</h2>') # Build the scene only if it has a working copy
            
            self._process_render = True
            return True

        elif hasWorkingCopy:    
            # self.render_type.ui(hidden=True)
            self.message.set(f'<h2>You need to publish the file before building the scene</h2>')
            self._process_render = False
            return True
    
    def get_buttons(self):
        if self._process_render:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]

    def get_dest_file(self,part):
        
        self.Log._log_view.append_text("Render Type : "+ part)
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            # print(file.name())
            if file.name() == (part+"_mp4"):
                dest_file = file
                print(dest_file)
                break
        if dest_file.has_working_copy() == False:
            self.Log._log_view.append_text("create working Copy !")
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()
        # print(self._dest_file_path) 

        return self._dest_file_path



    def write_script(self):


        if self._to_render == 'FULL_RES':
            self.Log._log_view.append_text("Rendering FULL_RES")
        if self._to_render == 'HALF_RES':
            self.Log._log_view.append_text("Rendering HALF_RES")
        if self._to_render == 'QUARTER_RES':
            self.Log._log_view.append_text("Rendering QUARTER_RES")
        

        render_scene_script_content = textwrap.dedent(f'''\
        import bpy

        # Set the output file path 
        output_file_path = "\\{self._dest_file_path}"

        # Set the rendering settings
        # bpy.context.scene.frame_start = start_frame
        # bpy.context.scene.frame_end = end_frame
        bpy.context.scene.render.filepath = output_file_path

        # Set the output format and codec options (modify as needed)
        bpy.context.scene.render.image_settings.file_format = 'FFMPEG'
        bpy.context.scene.render.ffmpeg.format = 'MPEG4'
        bpy.context.scene.render.ffmpeg.codec = 'H264'
        bpy.context.scene.render.ffmpeg.audio_codec = 'AAC'

        # Render the animation
        bpy.ops.render.render(animation=True)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'render_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(render_scene_script_content)

        # print(f"Render script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            HR = self._file.get_head_revision().get_path()
            

            self._to_render = self.render_type.get()
            self._dest_file_path = self.get_dest_file(self._to_render)
            if self._dest_file_path != "":
                script_path = self.write_script()
                self.Log._log_view.append_text("Rendering " + self.render_type.get())
                self.Log._log_view.append_text("Writting script in temp : " +  script_path)
                self.Log._log_view.append_text("Rendering scene : " + HR)
                return GenericRunAction.run(self, button)
                # return super(renderScene, self).run(button)
            else:
                print("Cannot find destination file to render to...")
 
class RIG_renderScene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    
    ICON = ("icons", "bake")
    _file = flow.Parent()
    _file_system = flow.Parent(2)
    _task = flow.Parent(3)
    _script_path = ""
    _process_render = False
    _to_render = ""
    _start_frame = 0
    _stop_frame = 0
    _dest_file_path = ""
    Log = None

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "RIG_2D_CHARS" : # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasRevision != None and hasWorkingCopy == False:

            self.message.set(f'<h2>Render this file ?</h2>') # Build the scene only if it has a working copy
            
            self._process_render = True
            return True

        elif hasWorkingCopy:    
            # self.render_type.ui(hidden=True)
            self.message.set(f'<h2>You need to publish the file before building the scene</h2>')
            self._process_render = False
            return True
    
    def get_buttons(self):
        if self._process_render:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]

    def get_dest_file(self):
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            # print(file.name())
            if file.name() == ("RIG_2D_RENDER_mp4"):
                dest_file = file
                print(dest_file)
                break
        if dest_file.has_working_copy() == False:
            self.Log._log_view.append_text("create working Copy !")
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Output Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()
        # print(self._dest_file_path) 

        return self._dest_file_path



    def write_script(self):


        if self._to_render == 'FULL_RES':
            self.Log._log_view.append_text("Rendering FULL_RES")
        if self._to_render == 'HALF_RES':
            self.Log._log_view.append_text("Rendering HALF_RES")
        if self._to_render == 'QUARTER_RES':
            self.Log._log_view.append_text("Rendering QUARTER_RES")
        

        render_scene_script_content = textwrap.dedent(f'''\
        import bpy

        # Set the output file path 
        output_file_path = "\\{self._dest_file_path}"

        # Set the rendering settings
        bpy.context.scene.frame_start = 1
        bpy.context.scene.frame_end = 220
        bpy.context.scene.render.filepath = output_file_path

        # Set the output format and codec options (modify as needed)
        bpy.context.scene.render.image_settings.file_format = 'FFMPEG'
        bpy.context.scene.render.ffmpeg.format = 'MPEG4'
        bpy.context.scene.render.ffmpeg.codec = 'H264'
        bpy.context.scene.render.ffmpeg.audio_codec = 'AAC'

        # Render the animation
        bpy.ops.render.render(animation=True)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'render_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(render_scene_script_content)

        # print(f"Render script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            HR = self._file.get_head_revision().get_path()
            
            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                self.Log._log_view.append_text("Writting script in temp : " +  script_path)
                self.Log._log_view.append_text("Rendering scene : " + HR)
                return GenericRunAction.run(self, button)
                # return super(renderScene, self).run(button)
            else:
                print("Cannot find destination file to render to...")
 
class BG_Render_Scene(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)

    ICON = ("icons", "bake")
    _file = flow.Parent()
    _file_system = flow.Parent(2)
    _task = flow.Parent(3)
    _script_path = ""
    _process_render = False
    _to_render = ""
    _start_frame = 0
    _stop_frame = 0
    _dest_file_path = ""
    Log = None
    cameras_to_render = []
    _additional_shots = []
    _frame_ranges = []
    HR_paths_scene_and_range = []

    def __init__(self, parent, session):
        wm = self.root().session()
        self.Log = wm.find_view("Log View")
        self.HR_paths_scene_and_range = []

    def get_cams(self, task, project):

        self._additional_shots = []
        self._frame_ranges = []

        pattern = re.compile(r'^EP\d{3}_SQ\d{3}_SH\d{4}$')
        comments = gazu.task.all_comments_for_task(task)
        if comments:
            for item in comments[-1]["checklist"]:
                if pattern.match(item["text"]):
                    self._additional_shots.append(item["text"])

            # Get the frame ranges of the additional shots
            for shot in self._additional_shots:
                ep, seq, sh = shot.split("_")
                episode = gazu.shot.get_episode_by_name(project, ep)
                sequence = gazu.shot.get_sequence_by_name(project,seq,episode)
                additional_shot = gazu.shot.get_shot_by_name(sequence, sh)    
                self._frame_ranges.append({"CAM": "CAM_"+seq+"_"+sh, "nb_frame": additional_shot["nb_frames"]})
            
            self._frame_ranges.append({"CAM": "CAM_"+self._seq_name+"_"+self._sh_name, "nb_frame": self._nb_frame})
            print(self._frame_ranges)
            return True
        else:
            self._frame_ranges.append({"CAM": "CAM_"+self._seq_name+"_"+self._sh_name, "nb_frame": self._nb_frame})
            print(self._frame_ranges)
            print("No comment found")
            return False

    def get_shot_info(self):
        # get episode sequence and shot name 
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(self.oid())]
        episode_oid, seq_oid, shot_oid = oids[-6:-3]
        
        self._ep_name = self.root().get_object(episode_oid).name()
        self._seq_name = self.root().get_object(seq_oid).name()
        self._sh_name = self.root().get_object(shot_oid).name()
        print(self._sh_name)

        if kitsu.current_user_logged_in(self):
            # print("User logged in...")
            project = gazu.project.get_project_by_name("EWILAN")
            episode = gazu.shot.get_episode_by_name(project, self._ep_name)
            sequence = gazu.shot.get_sequence_by_name(project,self._seq_name,episode)
            shot = gazu.shot.get_shot_by_name(sequence, self._sh_name)
            task_type = gazu.task.get_task_type_by_name(self._task.name())

            task = gazu.task.get_task_by_name(shot,task_type)
            self._nb_frame = shot["nb_frames"]

            shot_data = shot["data"]
            # Check if the shot has any "use_bg_from" data
            if "use_bg_from" in shot_data:
                print("shot has this data")
                # Check if the data is not none
                data = shot_data["use_bg_from"]
                if len(data)>1:
                    use_bg = shot["data"]["use_bg_from"]
                    return use_bg
                else:
                    self.get_cams(task, project)
                    return True
            else:
                self.get_cams(task, project)
                return True

    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        if file_type == "Works" and self._task.name() == "LAYOUT_BG": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        self.Log._log_view.clear()
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        self.HR_paths_scene_and_range = []
        if hasRevision != None and hasWorkingCopy == False:

            self.cameras_to_render = self.get_shot_info()

            if type(self.cameras_to_render) != bool:
                self.message.set(f'<h2>You cannot render this scene as it is made from {self.cameras_to_render}</h2>')
                self._process_render = False
                return False
            else:
                self.message.set(f'<h2>Cameras to render</h2>{self._frame_ranges}') # Build the scene only if it has a working copy
            
                self._process_render = True
                return True

        elif hasWorkingCopy:    
            
            self.message.set(f'<h2>You need to publish the file before building the scene</h2>')
            self._process_render = False
            return True
    
    def get_buttons(self):
        if self._process_render:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path]

    def write_script(self):

        render_scene_script_content = textwrap.dedent(f'''\
        import bpy

        def scene_exists(scene_name):
            for scene in bpy.data.scenes:
                if scene.name == scene_name:
                    return True
            return False

        for shot in {self.HR_paths_scene_and_range}:

            if scene_exists(shot["scene"]):
                scene_to_render = bpy.data.scenes[shot["scene"]] 
            else:
                scene_to_render = bpy.data.scenes["SCN_BUILD"]
            bpy.context.window.scene = scene_to_render

            scene = bpy.context.window.scene

            if scene.camera is None :
                cameras = [o for o in scene.objects if type(o.data).__name__ == "Camera"]
                print("%s cameras found." % len(cameras))
                scene.camera = cameras[0]

            # Set the output file path 
            output_file_path = shot["path"]

            # Set the rendering settings
            bpy.context.scene.frame_start = 1
            bpy.context.scene.frame_end = shot["range"]
            bpy.context.scene.render.filepath = output_file_path

            # Set the output format and codec options (modify as needed)
            bpy.context.scene.render.image_settings.file_format = 'FFMPEG'
            bpy.context.scene.render.ffmpeg.format = 'MPEG4'
            bpy.context.scene.render.ffmpeg.codec = 'H264'
            bpy.context.scene.render.ffmpeg.audio_codec = 'AAC'

            # Render the animation
            bpy.ops.render.render(animation=True)                                         

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'render_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(render_scene_script_content)

        # print(f"Render script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def create_dest_files(self,cams):
        print(cams)
        
        for cam in cams:
            cam_name = cam["CAM"]
            scene_name = cam["CAM"].replace("CAM", "SCN")
            cam_range = cam["nb_frame"]
            print(cam)
            print(cam_name)
            theFile = self._task.files.add_file(cam_name, "mp4", display_name=cam_name+".mp4", base_name=cam_name, tracked=True, default_path_format='{film}/{sequence}/{shot}/{task}/{file_mapped_name}/OUTPUT/{project_code}_{film}_'f'{cam_name}_'+'{task_code}')
            theFile.file_type.set("Outputs")
            print(theFile)
            theFile.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            head_revision_name = self._file.get_head_revision().name()
            theFile.publish(revision_name=None, source_path=None, comment=('Rendered from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
            self.HR_paths_scene_and_range.append({"path" : theFile.get_head_revision().get_path() , "scene" : scene_name, "range" : cam_range})
        return True

    def get_dest_file(self, cams):
        file_exists = False
        for cam in cams:
            scene_name = cam["CAM"].replace("CAM", "SCN")
            file_name = cam["CAM"]+"_mp4"
            cam_range = cam["nb_frame"]
            task_output_files = self._task.get_files(file_type="Outputs")
            for file in task_output_files:
                if file.name() == file_name:
                    file_exists = True
                    file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
                    head_revision_name = self._file.get_head_revision().name()
                    file.publish(revision_name=None, source_path=None, comment=('Rendered from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
                    self.HR_paths_scene_and_range.append({"path" : file.get_head_revision().get_path() , "scene" : scene_name, "range" : cam_range})

        return file_exists

    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':
            
            if self._process_render:
                print("Let's go for rendering !")
                # check if dest file exists
                if self.get_dest_file(self._frame_ranges):
                    print("it exists")
                else:
                    self.create_dest_files(self._frame_ranges)

                print(self._frame_ranges)
                print(self.HR_paths_scene_and_range)
                script_path = self.write_script()
                print("Writting script in temp : ", script_path)
                return GenericRunAction.run(self, button)


def bg_render_scene(parent):
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(BG_Render_Scene) # Get the relation
        relation.name = 'Render_Scene_BG' # Explicitely definining its name
        return relation # Return relation

def rig_render_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(RIG_renderScene) # Get the relation
        relation.name = 'Render_Scene_RIG' # Explicitely definining its name
        return relation # Return relation

def ms_render_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(MS_renderScene) # Get the relation
        relation.name = 'Render_Scene_MS' # Explicitely definining its name
        return relation # Return relation
    
def ms_props_render_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(MS_PROPS_renderScene) # Get the relation
        relation.name = 'Render_Scene_MS_Props' # Explicitely definining its name
        return relation # Return relation
    
def an_render_scene(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(AN_renderScene) # Get the relation
        relation.name = 'Render_Scene_AN' # Explicitely definining its name
        return relation # Return relation

def install_extensions(session):
    return {
        'render_scene_action': [
            ms_render_scene,
            an_render_scene,
            rig_render_scene,
            ms_props_render_scene,
            bg_render_scene
        ]
    }
