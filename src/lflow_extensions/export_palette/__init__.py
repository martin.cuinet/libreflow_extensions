import os
import tempfile
import textwrap
from kabaret import flow
from libreflow.baseflow.file import File, OpenWithAction, GenericRunAction

class ExportPalette(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    
    ICON = ("icons", "palette")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _script_path = ""
    _export_palette = False
    _dest_file_path = ""


    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]
        asset_type = type_oid.rsplit('/', 1)[-1]
        if file_type == "Works" and self._task.name() == "PALETTE" and (asset_type == "CHARACTERS" or asset_type == "CREATURES"): # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasWorkingCopy == False and hasRevision != None:

            self.message.set(f'<h2>Export Palette ?</h2>') # export palette only if it has a working copy
            self._export_palette = True
            return True

        elif hasWorkingCopy == True:    
            self.message.set(f'<h2>You need to publish the scene before exporting the palette</h2>')
            self._export_palette = False
            return True
    
    def get_buttons(self):
        if self._export_palette:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path] 

    def get_dest_file(self):
        
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            # print(file.name())
            if file.name() == ("PALETTE_json"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # print("Working copy created") 
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Palette Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()
        # print(self._dest_file_path) 

        return self._dest_file_path

    def write_script(self):


        render_scene_script_content = textwrap.dedent(f'''\
        import bpy

        bpy.ops.scene.export_palette(filepath="\\{self._dest_file_path}", check_existing = True, use_relative_path = True)
                                        

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'render_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(render_scene_script_content)

        print(f"Export palette script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def link_ref(self):
        # Get the current ASSET oid
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(self.oid())]
        asset_oid = oids[-4]

        #  Get the asset's RIG_2D_CHARS task 
        RIG_task_oid= asset_oid + "/tasks/RIG_2D_CHARS"

        try:
            RIG = self.root().get_object(RIG_task_oid)

            # check if the RIG task already has the ref file
            files = RIG.get_files(include_links=True, file_type=None, primary_only=False)
            for file in files:
                if file.name() == "PALETTE_json":
                    print("Task ", RIG.name(), " already has the reference file set up")
                    return


            task_has_output = False
            #  Get the asset's GPMAT's Output file
            files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
            for file in files:
                if file.name() == ("PALETTE_json"):
                    ref_file = file
                    task_has_output= True
                    break
                    
            if task_has_output:
                print("PALETTE output found : ", ref_file)
                RIG.file_refs.add_ref(ref_file.oid(), "Inputs")
                self._task.touch()
                RIG.touch()
                print("Supposed to be added already...")
            else:
                print("PLT output not found ")
        except:
            print("No need to link ref")


    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                self.link_ref()
                return GenericRunAction.run(self, button)
                # return super(ExportPalette, self).run(button)
            else:
                print("Cannot find destination file to render to...")
            
class ExportPalette_Props(OpenWithAction):

    revision_name = flow.Param().ui(hidden=True)
    
    ICON = ("icons", "palette")
    _file = flow.Parent()
    _task = flow.Parent(3)
    _script_path = ""
    _export_palette = False
    _dest_file_path = ""


    def allow_context(self, context):
        file_type = self._file.file_type.get() # get the file type of the selected file
        oids = [oid for _, oid in self._file.root().session().cmds.Flow.split_oid(self.oid())]
        lib_oid, type_oid, asset_oid = oids[-6:-3]
        asset_type = type_oid.rsplit('/', 1)[-1]
        if file_type == "Works" and self._task.name() == "PALETTE" and asset_type == "PROPS": # only allows the action if the file is of type "Works"
            return True
        else:
            return False

    def needs_dialog(self):
        hasWorkingCopy = self._file.has_working_copy()
        hasRevision = self._file.get_head_revision()
        if hasWorkingCopy == False and hasRevision != None:

            self.message.set(f'<h2>Export Palette ?</h2>') # export palette only if it has a working copy
            self._export_palette = True
            return True

        elif hasWorkingCopy == True:    
            self.message.set(f'<h2>You need to publish the scene before exporting the palette</h2>')
            self._export_palette = False
            return True
    
    def get_buttons(self):
        if self._export_palette:
            return ['Cancel', 'Ok']
        else:
            return['Go it']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        HR = self._file.get_head_revision()
        return [ HR.get_path(),'--background', '--python', self._script_path] 

    def get_dest_file(self):
        
        
        files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
        for file in files:
            # print(file.name())
            if file.name() == ("PALETTE_json"):
                dest_file = file
                break
        if dest_file.has_working_copy() == False:
            dest_file.create_working_copy(from_revision=None, source_path=None, user_name=None, path_format=None)
            # print("Working copy created") 
            # get the revision name from which the output is updated
            head_revision_name = self._file.get_head_revision().name()
            dest_file.publish(revision_name=None, source_path=None, comment=('Palette Updated from '+ head_revision_name), keep_editing=False, ready_for_sync=True, path_format=None)
        self._dest_file_path = dest_file.get_head_revision().get_path()
        # print(self._dest_file_path) 

        return self._dest_file_path

    def write_script(self):


        render_scene_script_content = textwrap.dedent(f'''\
        import bpy

        bpy.ops.scene.export_palette(filepath="\\{self._dest_file_path}", check_existing = True, use_relative_path = True)
                                        

        ''')

        temp_folder = tempfile.gettempdir()
        render_scene_script_path = os.path.join(temp_folder, 'render_scene_script.py')
        with open(render_scene_script_path, 'w') as file:
            file.write(render_scene_script_content)

        print(f"Export palette script has been created and saved at: {render_scene_script_path}")
        self._script_path = render_scene_script_path
        return render_scene_script_path

    def link_ref(self):
        # Get the current ASSET oid
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(self.oid())]
        asset_oid = oids[-4]

        #  Get the asset's RIG_2D_PROPS task 
        RIG_task_oid= asset_oid + "/tasks/RIG_2D_PROPS"

        try:
            RIG = self.root().get_object(RIG_task_oid)

            # check if the RIG task already has the ref file
            files = RIG.get_files(include_links=True, file_type=None, primary_only=False)
            for file in files:
                if file.name() == "PALETTE_json":
                    print("Task ", RIG.name(), " already has the reference file set up")
                    return


            task_has_output = False
            #  Get the asset's GPMAT's Output file
            files = self._task.get_files( include_links=False, file_type="Outputs", primary_only=True)
            for file in files:
                if file.name() == ("PALETTE_json"):
                    ref_file = file
                    task_has_output= True
                    break
                    
            if task_has_output:
                print("PALETTE output found : ", ref_file)
                RIG.file_refs.add_ref(ref_file.oid(), "Inputs")
                self._task.touch()
                RIG.touch()
                print("Supposed to be added already...")
            else:
                print("PLT output not found ")
        except:
            print("No need to link ref")


    def run(self,button):

        if button == 'Cancel':
            return
        
        if button == 'Got it':
            return
        
        if button == 'Ok':

            self._dest_file_path = self.get_dest_file()
            if self._dest_file_path != "":
                script_path = self.write_script()
                self.link_ref()
                return GenericRunAction.run(self, button)
                # return super(ExportPalette, self).run(button)
            else:
                print("Cannot find destination file to render to...")



def export_palette(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(ExportPalette) # Get the relation
        relation.name = 'Export_Palette' # Explicitely definining its name
        return relation # Return relation
    
def export_palette_props(parent):
    
    if isinstance(parent, File): # Check if the parent is of type File
        relation = flow.Child(ExportPalette_Props) # Get the relation
        relation.name = 'Export_Palette_props' # Explicitely definining its name
        return relation # Return relation

def install_extensions(session):
    return {
        'export_palette_action': [
            export_palette,
            export_palette_props
        ]
    }
